package com.mobisquid.user;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import com.general.files.ExecuteWebServerUrl;
import com.general.files.GeneralFunctions;
import com.general.files.StartActProcess;
import com.utils.CommonUtilities;
import com.utils.Utils;
import com.view.MButton;
import com.view.MTextView;
import com.view.MaterialRippleLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class UberXSelectServiceActivity extends AppCompatActivity {


    public MButton btn_type2_rideNow;
    public MButton btn_type2_rideLater;
    GeneralFunctions generalFunc;
    MTextView titleTxt, noDataTxt;
    ImageView backImgView;
    LinearLayout serviceLayout;
    RelativeLayout selectServicePage;
    MButton continueBtn;
    String iVehicleCategoryId = "";
    String USER_PROFILE_JSON = "";
    String selectedVehicleTypeId = "1";
    String selectvVehicleType = "";
    String selectvVehiclePrice = "";
    int selectedVehiclePosition = 0;
    ArrayList<View> listOfView = new ArrayList<>();
    boolean isclick = false;
    int QUANTITYdata = 0;
    String QUANTITYPrice = "";
    String FareType = "";
    LinearLayout rideLaterArea;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uber_xselect_service);
        generalFunc = new GeneralFunctions(getActContext());

        iVehicleCategoryId = getIntent().getStringExtra("iVehicleCategoryId");
        USER_PROFILE_JSON = generalFunc.retrieveValue(CommonUtilities.USER_PROFILE_JSON);
        titleTxt = (MTextView) findViewById(R.id.titleTxt);
        noDataTxt = (MTextView) findViewById(R.id.noDataTxt);

        noDataTxt.setText(generalFunc.retrieveLangLBl("No any service Data found.", "LBL_NO_SERVICE_DATA_TXT"));

        selectServicePage = (RelativeLayout) findViewById(R.id.selectServicePage);
        backImgView = (ImageView) findViewById(R.id.backImgView);
        serviceLayout = (LinearLayout) findViewById(R.id.servicesAreaLayout);
        continueBtn = (MButton) findViewById(R.id.continueBtn);
        continueBtn.setText(generalFunc.retrieveLangLBl("Continue", "LBL_CONTINUE_BTN"));
        btn_type2_rideNow = ((MaterialRippleLayout) findViewById(R.id.btn_type2_rideNow)).getChildView();
        btn_type2_rideLater = ((MaterialRippleLayout) findViewById(R.id.btn_type2_rideLater)).getChildView();
        rideLaterArea = (LinearLayout) findViewById(R.id.rideLaterArea);


        btn_type2_rideNow.setText(generalFunc.retrieveLangLBl("Book Now", "LBL_BOOK_NOW"));
        btn_type2_rideLater.setText(generalFunc.retrieveLangLBl("Book Later", "LBL_BOOK_LATER"));

        btn_type2_rideLater.setOnClickListener(new setOnClickList());
        btn_type2_rideNow.setOnClickListener(new setOnClickList());
        continueBtn.setOnClickListener(new setOnClickList());

        setData();
        intializeServiceCategoryTypes(iVehicleCategoryId);


        if (generalFunc.getJsonValue("RIDE_LATER_BOOKING_ENABLED", USER_PROFILE_JSON).equalsIgnoreCase("Yes")) {
            rideLaterArea.setVisibility(View.VISIBLE);
        } else {
            rideLaterArea.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        USER_PROFILE_JSON = generalFunc.retrieveValue(CommonUtilities.USER_PROFILE_JSON);
        isclick = false;
    }

    private void setData() {
        titleTxt.setText(generalFunc.retrieveLangLBl("", "LBL_SELECT_SERVICE"));
        backImgView.setOnClickListener(new setOnClickList());
    }

    private void intializeServiceCategoryTypes(String iVehicleCategoryId) {

        final ArrayList<View> listForViews = new ArrayList<>();
        final LayoutInflater mInflater = (LayoutInflater)
                getActContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        noDataTxt.setVisibility(View.GONE);
        HashMap<String, String> parameters = new HashMap<String, String>();

        parameters.put("type", "getServiceCategoryTypes");
        parameters.put("iVehicleCategoryId", iVehicleCategoryId);
        parameters.put("userId", generalFunc.getMemberId());
        parameters.put("vLatitude", getIntent().getStringExtra("latitude"));
        parameters.put("vLongitude", getIntent().getStringExtra("longitude"));

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(getActContext(), parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setCancelAble(false);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {


                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = generalFunc.checkDataAvail(CommonUtilities.action_str, responseString);

                    boolean viewMoreSet = false;
                    if (isDataAvail == true) {
                        final JSONArray mainCataArray = generalFunc.getJsonArray("message", responseString);

                        if (!generalFunc.getJsonValue("vCategoryTitle", responseString.toString()).trim().equals("")) {
                            ((MTextView) findViewById(R.id.vCategoryTitleTxtView)).setText(Html.fromHtml(generalFunc.getJsonValue("vCategoryTitle", responseString.toString()).replace("\r\n", "<br/>")));
                        } else {
                            (findViewById(R.id.vCategoryTitleTxtView)).setVisibility(View.GONE);
                        }

                        if (!generalFunc.getJsonValue("vCategoryDesc", responseString.toString()).trim().equals("")) {
                            ((MTextView) findViewById(R.id.descTxtView)).setText(Html.fromHtml(generalFunc.getJsonValue("vCategoryDesc", responseString.toString()).replace("\r\n", "<br/>")));

                            if (viewMoreSet == false) {
                                generalFunc.makeTextViewResizable((MTextView) findViewById(R.id.descTxtView), 6, "...\n+ " + generalFunc.retrieveLangLBl("View More", "LBL_VIEW_MORE_TXT"), true);
                                viewMoreSet = true;
                            }
                        } else {
                            (findViewById(R.id.descTxtView)).setVisibility(View.GONE);
                        }

                        for (int i = 0; i < mainCataArray.length(); i++) {
                            final HashMap<String, String> selectAllServiceMap = new HashMap<String, String>();
                            final JSONObject mainCataObject = generalFunc.getJsonObject(mainCataArray, i);

                            String ALLOW_SERVICE_PROVIDER_AMOUNT = generalFunc.getJsonValue("ALLOW_SERVICE_PROVIDER_AMOUNT", mainCataObject.toString());

                            ((MTextView) findViewById(R.id.vCategoryTitleTxtView)).setText(Html.fromHtml(generalFunc.getJsonValue("vCategoryTitle", mainCataObject.toString())));


                            ((MTextView) findViewById(R.id.selectServiceTxtView)).setText(getIntent().getStringExtra("vCategoryName"));

                            final View view = mInflater.inflate(R.layout.item_service_catagory_design, null);
                            final RelativeLayout countingArea;

                            final ImageView leftImgView = (ImageView) view.findViewById(R.id.leftImgView);
                            ImageView rightImgView = (ImageView) view.findViewById(R.id.rightImgView);
                            final MTextView QTYTxtView = (MTextView) view.findViewById(R.id.QTYTxtView);
                            final MTextView QTYNumberTxtView = (MTextView) view.findViewById(R.id.QTYNumberTxtView);
                            final MTextView priceTxtView = (MTextView) view.findViewById(R.id.priceTxtView);

//                          FARE DETAILS AREA START

                            final LinearLayout fareDetailArea = (LinearLayout) view.findViewById(R.id.fareDetailArea);

                            String eAllowQty = generalFunc.getJsonValue("eAllowQty", mainCataObject.toString());
                            if (!generalFunc.getJsonValue("eFareType", mainCataObject.toString()).equalsIgnoreCase("Fixed")) {
                                eAllowQty = "No";
                            }
                            final String iMaxQty = generalFunc.getJsonValue("iMaxQty", mainCataObject.toString());

                            String fPricePerKM = generalFunc.getJsonValue("fPricePerKM", mainCataObject.toString());
                            String fPricePerMin = generalFunc.getJsonValue("fPricePerMin", mainCataObject.toString());
                            String iBaseFare = generalFunc.getJsonValue("iBaseFare", mainCataObject.toString());
                            String iMinFare = generalFunc.getJsonValue("iMinFare", mainCataObject.toString());

                            ((MTextView) view.findViewById(R.id.fPricePerKMValueTxtView)).setText(fPricePerKM);
                            ((MTextView) view.findViewById(R.id.fPricePerMinValueTxtView)).setText(fPricePerMin);
                            ((MTextView) view.findViewById(R.id.iBaseFareValueTxtView)).setText(iBaseFare);
                            ((MTextView) view.findViewById(R.id.iMinFareValueTxtView)).setText(iMinFare);


                            String userProfileJson=generalFunc.retrieveValue(CommonUtilities.USER_PROFILE_JSON);
                            if (generalFunc.getJsonValue("eUnit", userProfileJson).equals("KMs")) {
                                ((MTextView) view.findViewById(R.id.fPricePerKMTxt)).setText(generalFunc.retrieveLangLBl("", "LBL_PRICE_PER_KM"));
                            }
                            else
                            {
                                ((MTextView) view.findViewById(R.id.fPricePerKMTxt)).setText(generalFunc.retrieveLangLBl("", "LBL_PRICE_PER_MILES"));
                            }
                            ((MTextView)view.findViewById(R.id.fPricePerMinTxt)).setText(generalFunc.retrieveLangLBl("","LBL_PRICE_PER_MINUTE"));
                            ((MTextView)view.findViewById(R.id.iBaseFareTxt)).setText(generalFunc.retrieveLangLBl("","LBL_BASE_FARE_SMALL_TXT"));
                            ((MTextView)view.findViewById(R.id.iMinFareTxt)).setText(generalFunc.retrieveLangLBl("","LBL_MIN_FARE"));

                            Utils.printLog("ALLOW_SERVICE_PROVIDER_AMOUNT", "::" + ALLOW_SERVICE_PROVIDER_AMOUNT);
                            if (generalFunc.getJsonValue("eFareType", mainCataObject.toString()).equalsIgnoreCase("Regular")) {
                                priceTxtView.setVisibility(View.GONE);
                            } else if (ALLOW_SERVICE_PROVIDER_AMOUNT.equalsIgnoreCase("No")) {
                                Utils.printLog("priceTxtView", ":: VISIBLE");
                                priceTxtView.setVisibility(View.VISIBLE);
                            } else {
                                priceTxtView.setVisibility(View.GONE);
                            }
//                          OVER FARE DETAILS AREA

                            ((MTextView) view.findViewById(R.id.fareHeadingTxtView)).setText("" + generalFunc.retrieveLangLBl("Fare Detail", "LBL_FARE_DETAIL_TXT"));

                            final RadioButton catagoryRadioBtn = (RadioButton) view.findViewById(R.id.catagoryRadioBtn);
                            catagoryRadioBtn.setText(generalFunc.getJsonValue("vVehicleType", mainCataObject.toString()));
                            countingArea = (RelativeLayout) view.findViewById(R.id.countingArea);
                            final LinearLayout uberXServiceAreaLayout = (LinearLayout) view.findViewById(R.id.uberXServiceAreaLayout);

                            QTYNumberTxtView.setText("1");
                            QTYTxtView.setText("" + generalFunc.retrieveLangLBl("QTY", "LBL_QTY_TXT"));
                            leftImgView.setImageResource(R.mipmap.ic_left_disabled_rounded);

                            if (generalFunc.getJsonValue("eFareType", mainCataObject.toString()).equalsIgnoreCase("Fixed")) {
                                priceTxtView.setText(generalFunc.getJsonValue("fFixedFare", mainCataObject.toString()));
                            } else if (generalFunc.getJsonValue("eFareType", mainCataObject.toString()).equalsIgnoreCase("Hourly")) {
                                Utils.printLog("priceTxtView val", "::" + generalFunc.getJsonValue("fPricePerHour", mainCataObject.toString()) + "/" + generalFunc.retrieveLangLBl("hour", "LBL_HOUR"));
                                priceTxtView.setText(
                                        generalFunc.getJsonValue("fPricePerHour", mainCataObject.toString()) + "/" + generalFunc.retrieveLangLBl("hour", "LBL_HOUR"));
                            }


                            rightImgView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {

                                        String qtytempdata = QTYNumberTxtView.getText().toString();
                                        String filterdataqty = qtytempdata.replace(generalFunc.getJsonValue("vSymbol", mainCataObject.toString()), "");
                                        int QUANTITY = Integer.parseInt(filterdataqty);
                                        QUANTITYdata = QUANTITY;


                                        if (QUANTITY < Integer.parseInt(iMaxQty)) {
                                            if (QUANTITY >= 1) {
                                                QUANTITY = QUANTITY + 1;

                                                String tempdata = generalFunc.getJsonValue("fFixedFare_value", mainCataObject.toString());
                                                String filterdata = tempdata.replace(generalFunc.getJsonValue("vSymbol", mainCataObject.toString()), "");
                                                priceTxtView.setText(generalFunc.getJsonValue("vSymbol", mainCataObject.toString()) +
                                                        (QUANTITY * GeneralFunctions.parseDoubleValue(0.0, filterdata)) + "");
                                                QTYTxtView.setText(generalFunc.retrieveLangLBl("QTY", "LBL_QTY_TXT"));
                                                QTYNumberTxtView.setText("" + QUANTITY);
                                                leftImgView.setImageResource(R.mipmap.ic_left_arrow_rounded);
                                                leftImgView.setEnabled(true);
                                                selectAllServiceMap.put("fFixedFare", priceTxtView.getText().toString());
                                                selectAllServiceMap.put("QUANTITY", QTYNumberTxtView.getText().toString());

                                                QUANTITYdata = QUANTITY;
                                                QUANTITYPrice = generalFunc.getJsonValue("vSymbol", mainCataObject.toString()) +
                                                        (QUANTITY * GeneralFunctions.parseDoubleValue(0.0, filterdata)) + "";
                                            }
                                        } else {

                                            generalFunc.showMessage(generalFunc.getCurrentView((Activity) getActContext()),
                                                    generalFunc.retrieveLangLBl("Maximum quantity is reached for this service", "LBL_QUANTITY_CLOSED_TXT"));

                                        }
                                    } catch (Exception e) {
                                        Utils.printLog("Exception::", e.toString());

                                    }
                                }
                            });

                            leftImgView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {

                                        String filterdataqty = QTYNumberTxtView.getText().toString();
                                        int QUANTITY = Integer.parseInt(filterdataqty);
                                        //    int QUANTITY = Integer.parseInt(QTYNumberTxtView.getText().toString());

                                        QUANTITYdata = QUANTITY;
                                        if (QUANTITY > 1) {
                                            QUANTITY = QUANTITY - 1;

                                            String tempdata = generalFunc.getJsonValue("fFixedFare_value", mainCataObject.toString());
                                            String filterdata = tempdata.replace(generalFunc.getJsonValue("vSymbol", mainCataObject.toString()), "");
                                            priceTxtView.setText(generalFunc.getJsonValue("vSymbol", mainCataObject.toString()) +
                                                    (QUANTITY * GeneralFunctions.parseDoubleValue(0.0, filterdata)) + "");
                                            QTYTxtView.setText(generalFunc.retrieveLangLBl("QTY", "LBL_QTY_TXT"));
                                            QTYNumberTxtView.setText("" + QUANTITY);
                                            leftImgView.setImageResource(R.mipmap.ic_left_arrow_rounded);
                                            selectAllServiceMap.put("fFixedFare", priceTxtView.getText().toString());
                                            selectAllServiceMap.put("QUANTITY", QTYNumberTxtView.getText().toString());

                                            QUANTITYdata = QUANTITY;
                                            QUANTITYPrice = generalFunc.getJsonValue("vSymbol", mainCataObject.toString()) +
                                                    (QUANTITY * GeneralFunctions.parseDoubleValue(0.0, filterdata)) + "";

                                        } else {
                                            QTYTxtView.setEnabled(false);
                                            leftImgView.setImageResource(R.mipmap.ic_left_disabled_rounded);
                                        }
                                    } catch (Exception e) {

                                    }
                                }
                            });
                            listForViews.add(view);

                            catagoryRadioBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    uberXServiceAreaLayout.performClick();
                                }
                            });

                            final int finalI = i;
                            final String finalEAllowQty = eAllowQty;
                            uberXServiceAreaLayout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    selectedVehicleTypeId = generalFunc.getJsonValue("iVehicleTypeId", mainCataObject.toString());
                                    selectvVehicleType = generalFunc.getJsonValue("vVehicleType", mainCataObject.toString());
                                    FareType = generalFunc.getJsonValue("eFareType", mainCataObject.toString());


                                    if (generalFunc.getJsonValue("eFareType", mainCataObject.toString()).equalsIgnoreCase(Utils.CabFaretypeFixed)) {
                                        selectvVehiclePrice = generalFunc.getJsonValue("fFixedFare", mainCataObject.toString());
                                    } else if (generalFunc.getJsonValue("eFareType", mainCataObject.toString()).equalsIgnoreCase(Utils.CabFaretypeHourly)) {
                                        selectvVehiclePrice = generalFunc.getJsonValue("fPricePerHour", mainCataObject.toString()) + "/" + generalFunc.retrieveLangLBl("hour", "LBL_HOUR");

                                    }


                                    selectedVehiclePosition = finalI;

                                    for (int r = 0; r < listForViews.size(); r++) {
                                        ((RadioButton) listForViews.get(r).findViewById(R.id.catagoryRadioBtn)).setChecked(false);
                                        (listForViews.get(r).findViewById(R.id.countingArea)).setVisibility(View.GONE);
                                        (listForViews.get(r).findViewById(R.id.fareDetailArea)).setVisibility(View.GONE);
                                    }
                                    if (generalFunc.getJsonValue("eFareType", mainCataObject.toString()).equalsIgnoreCase("Regular")) {
                                        fareDetailArea.setVisibility(View.VISIBLE);

                                    } else {
                                        fareDetailArea.setVisibility(View.GONE);
                                    }
                                    if (finalEAllowQty.equals("Yes")) {
                                        if (countingArea.getVisibility() == View.VISIBLE) {
                                            countingArea.setVisibility(View.GONE);
                                            catagoryRadioBtn.setChecked(false);
                                        } else {
                                            catagoryRadioBtn.setChecked(true);
                                            countingArea.setVisibility(View.VISIBLE);
                                        }
                                    } else {
                                        if (catagoryRadioBtn.isChecked() == false) {
                                            catagoryRadioBtn.setChecked(true);
                                        } else {
                                            catagoryRadioBtn.setChecked(false);
                                        }

                                    }
                                }
                            });
                            selectAllServiceMap.put("vVehicleType", generalFunc.getJsonValue("vVehicleType", mainCataObject.toString()));
                            selectAllServiceMap.put("iVehicleTypeId", generalFunc.getJsonValue("iVehicleTypeId", mainCataObject.toString()));

                            serviceLayout.addView(view);

                            if (i == 0) {
                                uberXServiceAreaLayout.performClick();
                            }
                        }

                        listOfView.addAll(listForViews);
                    } else {
                        if (noDataTxt.getVisibility() == View.GONE) {
                            noDataTxt.setVisibility(View.VISIBLE);
                        }
                        generalFunc.showGeneralMessage("",
                                generalFunc.retrieveLangLBl("", generalFunc.getJsonValue("message", responseString)));
                    }
                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

    public Context getActContext() {
        return UberXSelectServiceActivity.this;
    }

    private void intializeServiceCategoryTypes(String iVehicleCategoryId, String eCheck, final boolean isBookLater, final Bundle bundle) {

        HashMap<String, String> parameters = new HashMap<String, String>();

        parameters.put("type", "getServiceCategoryTypes");
        parameters.put("iVehicleCategoryId", iVehicleCategoryId);
        parameters.put("userId", generalFunc.getMemberId());
        parameters.put("vLatitude", getIntent().getStringExtra("latitude"));
        parameters.put("vLongitude", getIntent().getStringExtra("longitude"));
        parameters.put("UserType", Utils.userType);
        parameters.put("eCheck", eCheck);

        final ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(getActContext(), parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setCancelAble(false);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {


                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = generalFunc.checkDataAvail(CommonUtilities.action_str, responseString);

                    if (isDataAvail) {

                        if (isBookLater) {
                            if (!isclick) {
                                isclick = true;
                                bundle.putBoolean("isufx", true);
                                bundle.putString("latitude", getIntent().getStringExtra("latitude"));
                                bundle.putString("longitude", getIntent().getStringExtra("longitude"));
                                bundle.putString("address", getIntent().getStringExtra("address"));
                                bundle.putString("type", Utils.CabReqType_Later);
                                bundle.putString("Quantityprice", QUANTITYPrice);


                                if (FareType.equals(Utils.CabFaretypeRegular)) {

                                    // bundle.putString("SelectvVehicleType", Utils.CabFaretypeRegular);
                                    bundle.putString("SelectvVehiclePrice", "");
                                    bundle.putString("iUserAddressId", "");
                                    bundle.putString("Quantity", "");
                                    bundle.putString("Quantityprice", "");
                                    bundle.putString("Sdate", "");
                                    bundle.putString("Stime", "");
                                    new StartActProcess(getActContext()).startActWithData(ScheduleDateSelectActivity.class, bundle);


                                } else {
                                    if (generalFunc.getJsonValue("ToTalAddress", USER_PROFILE_JSON).equalsIgnoreCase("0")) {

                                        // new StartActProcess(getActContext()).startActWithData(MainActivity.class, bundle);
                                        new StartActProcess(getActContext()).startActWithData(AddAddressActivity.class, bundle);


                                    } else {

                                        // new StartActProcess(getActContext()).startActWithData(MainActivity.class, bundle);
                                        new StartActProcess(getActContext()).startActWithData(ListAddressActivity.class, bundle);
                                    }
                                }


                            }
                        } else {
                            if (!isclick) {
                                isclick = true;
                                bundle.putBoolean("isufx", true);
                                bundle.putString("latitude", getIntent().getStringExtra("latitude"));
                                bundle.putString("longitude", getIntent().getStringExtra("longitude"));
                                bundle.putString("address", getIntent().getStringExtra("address"));
                                bundle.putString("type", Utils.CabReqType_Now);
                                bundle.putString("Quantityprice", QUANTITYPrice);
                                bundle.putString("selType",Utils.CabGeneralType_UberX);


                                if (FareType.equals(Utils.CabFaretypeRegular)) {

                                    // bundle.putString("SelectvVehicleType", Utils.CabFaretypeRegular);
                                    bundle.putString("SelectvVehiclePrice", "");
                                    bundle.putString("iUserAddressId", "");
                                    bundle.putString("Quantity", "");
                                    bundle.putString("Quantityprice", "");
                                    bundle.putString("Sdate", "");
                                    bundle.putString("Stime", "");
                                    new StartActProcess(getActContext()).startActWithData(MainActivity.class, bundle);


                                } else {

                                    if (generalFunc.getJsonValue("ToTalAddress", USER_PROFILE_JSON).equalsIgnoreCase("0")) {
                                        new StartActProcess(getActContext()).startActWithData(AddAddressActivity.class, bundle);
                                    } else {
                                        new StartActProcess(getActContext()).startActWithData(ListAddressActivity.class, bundle);
                                    }
                                }
                            }
                        }
                    } else {

                        generalFunc.showGeneralMessage("", generalFunc.retrieveLangLBl("", generalFunc.getJsonValue("message", responseString)));
                    }
                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

    public class setOnClickList implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            try {
                Utils.hideKeyboard(getActContext());
                Bundle bundle = new Bundle();
                //    bundle.putString("USER_PROFILE_JSON", USER_PROFILE_JSON);
                bundle.putString("SelectedVehicleTypeId", selectedVehicleTypeId);
                bundle.putString("latitude", getIntent().getStringExtra("latitude"));
                bundle.putString("longitude", getIntent().getStringExtra("longitude"));
                bundle.putString("SelectvVehicleType", selectvVehicleType);
                bundle.putString("SelectvVehiclePrice", selectvVehiclePrice);


                String quantity = "1";
                if (listOfView != null && listOfView.size() > 0 && listOfView.get(selectedVehiclePosition) != null &&
                        listOfView.get(selectedVehiclePosition).findViewById(R.id.QTYNumberTxtView) != null) {
                    quantity = ((MTextView) listOfView.get(selectedVehiclePosition).findViewById(R.id.QTYNumberTxtView)).getText().toString();
                    QUANTITYPrice = ((MTextView) listOfView.get(selectedVehiclePosition).findViewById(R.id.priceTxtView)).getText().toString();
                }
                if (quantity.equals("0")) {

                    quantity = "1";
                }
                if (listOfView != null && ((RelativeLayout) listOfView.get(selectedVehiclePosition).findViewById(R.id.countingArea)).getVisibility() == View.GONE) {
                    quantity = "0";
                }
                bundle.putString("Quantity", quantity);


                if (v == backImgView) {
                    UberXSelectServiceActivity.super.onBackPressed();
                } else if (v == btn_type2_rideNow) {

                    if (!isclick) {
                        isclick = true;
                        bundle.putBoolean("isufx", true);
                        bundle.putString("latitude", getIntent().getStringExtra("latitude"));
                        bundle.putString("longitude", getIntent().getStringExtra("longitude"));
                        bundle.putString("address", getIntent().getStringExtra("address"));
                        bundle.putString("type", Utils.CabReqType_Now);
                        bundle.putString("Quantityprice", QUANTITYPrice);


                        if (FareType.equals(Utils.CabFaretypeRegular)) {

                            // bundle.putString("SelectvVehicleType", Utils.CabFaretypeRegular);
                            bundle.putString("SelectvVehiclePrice", "");
                            bundle.putString("iUserAddressId", "");
                            bundle.putString("Quantity", "");
                            bundle.putString("Quantityprice", "");
                            bundle.putString("Sdate", "");
                            bundle.putString("Stime", "");
                            new StartActProcess(getActContext()).startActWithData(MainActivity.class, bundle);


                        } else {

                            if (generalFunc.getJsonValue("ToTalAddress", USER_PROFILE_JSON).equalsIgnoreCase("0")) {
                                new StartActProcess(getActContext()).startActWithData(AddAddressActivity.class, bundle);
                            } else {
                                new StartActProcess(getActContext()).startActWithData(ListAddressActivity.class, bundle);
                            }
                        }

                    }

                } else if (v == btn_type2_rideLater) {

                    if (!isclick) {
                        isclick = true;
                        bundle.putBoolean("isufx", true);
                        bundle.putString("latitude", getIntent().getStringExtra("latitude"));
                        bundle.putString("longitude", getIntent().getStringExtra("longitude"));
                        bundle.putString("address", getIntent().getStringExtra("address"));
                        bundle.putString("type", Utils.CabReqType_Later);
                        bundle.putString("Quantityprice", QUANTITYPrice);


                        if (FareType.equals(Utils.CabFaretypeRegular)) {

                            // bundle.putString("SelectvVehicleType", Utils.CabFaretypeRegular);
                            bundle.putString("SelectvVehiclePrice", "");
                            bundle.putString("iUserAddressId", "");
                            bundle.putString("Quantity", "");
                            bundle.putString("Quantityprice", "");
                            bundle.putString("Sdate", "");
                            bundle.putString("Stime", "");
                            new StartActProcess(getActContext()).startActWithData(ScheduleDateSelectActivity.class, bundle);

                        } else {
                            if (generalFunc.getJsonValue("ToTalAddress", USER_PROFILE_JSON).equalsIgnoreCase("0")) {

                                // new StartActProcess(getActContext()).startActWithData(MainActivity.class, bundle);
                                new StartActProcess(getActContext()).startActWithData(AddAddressActivity.class, bundle);

                            } else {

                                // new StartActProcess(getActContext()).startActWithData(MainActivity.class, bundle);
                                new StartActProcess(getActContext()).startActWithData(ListAddressActivity.class, bundle);
                            }
                        }


                    }

                }
            } catch (Exception e) {
                UberXSelectServiceActivity.super.onBackPressed();
            }


        }
    }

}
