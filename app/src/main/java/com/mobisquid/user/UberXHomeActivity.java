package com.mobisquid.user;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.adapter.files.UberXHomeActBannerAdapter;
import com.adapter.files.UberXMainCatagoryAdapter;
import com.adapter.files.UberXSubCategoryListAdapter;
import com.general.files.AddDrawer;
import com.general.files.ExecuteWebServerUrl;
import com.general.files.GeneralFunctions;
import com.general.files.StartActProcess;
import com.squareup.picasso.Picasso;
import com.utils.CommonUtilities;
import com.utils.Utils;
import com.view.LoopingCirclePageIndicator;
import com.view.MTextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class UberXHomeActivity extends AppCompatActivity {

    public String userProfileJson = "";
    public LinearLayout subCatagoryListLayout;
    public LinearLayout subCatagoryLayout;
    public String parentId = "";
    public String catagoryId = "";
    CollapsingToolbarLayout collapsingToolbarLayout;
    GeneralFunctions generalFunc;
    MTextView headerLocAddressTxt, LocStaticTxt;
    ViewGroup mFrameLayout;
    RecyclerView mainCatagoryrecyclerView;
    ArrayList<HashMap<String, String>> subCatList = new ArrayList<>();
    LinearLayout uberXHeaderLayout;
    ProgressBar uberXHomePageLoader, subCatagoryloader;
    UberXSubCategoryListAdapter listCategoryAdapter;
    ListView selectServiceCategoryListView;
    ImageView backArrowImgView;
    UberXMainCatagoryAdapter uberXMainCatagoryAdapter;
    AddDrawer addDrawer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uber_xhome);
        generalFunc = new GeneralFunctions(getActContext());

        uberXHomePageLoader = (ProgressBar) findViewById(R.id.uberXHomePageLoader);

        uberXHeaderLayout = (LinearLayout) findViewById(R.id.uberXHeaderLayout);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        LocStaticTxt = (MTextView) findViewById(R.id.LocStaticTxt);
        mFrameLayout = (ViewGroup) findViewById(R.id.pagesContainer);
        mainCatagoryrecyclerView = (RecyclerView) findViewById(R.id.mainCatagoryrecyclerView);
        subCatagoryListLayout = (LinearLayout) findViewById(R.id.subCatagoryListLayout);
        headerLocAddressTxt = (MTextView) findViewById(R.id.headerLocAddressTxt);
        userProfileJson = generalFunc.retrieveValue(CommonUtilities.USER_PROFILE_JSON);



        addDrawer = new AddDrawer(getActContext(), userProfileJson,false);

        headerLocAddressTxt.setOnClickListener(new setOnClickLst());
        setDataSubCatagory();
        setData();

        addDrawer.setItemClickList(() -> configCategoryView());
        showLoader();
        collapsingToolbarLayout.setTitle("");
        parentId = generalFunc.getJsonValue(CommonUtilities.UBERX_PARENT_CAT_ID, userProfileJson);

        getBanners();


        initializeSubCatagoryWithoutBanner(catagoryId);

        if (parentId.equalsIgnoreCase("0")) {
            intializeMainCatagory();
        } else {
            intializeSubCatagory();
        }

        if (uberXHeaderLayout.getVisibility() == View.GONE) {
            uberXHeaderLayout.setVisibility(View.VISIBLE);
            (findViewById(R.id.headerLogo)).setVisibility(View.GONE);
        }
        uberXHeaderLayout.setOnClickListener(new setOnClickLst());

    }

    private void setDataSubCatagory() {
        subCatagoryLayout = (LinearLayout) findViewById(R.id.subCatagoryLayout);
        selectServiceCategoryListView = (ListView) findViewById(R.id.selectServiceCategoryListView);
        ((MTextView) findViewById(R.id.selectServiceTxt)).setText("Select Service");
    }

    public void initializeSubCatagoryWithoutBanner(String catagoryId) {
        final ArrayList<HashMap<String, String>> getSubCatagoryWithoutBannerList = getSubCatagoryData(catagoryId);

        listCategoryAdapter = new UberXSubCategoryListAdapter(getActContext(), getSubCatagoryWithoutBannerList);
        selectServiceCategoryListView.setAdapter(listCategoryAdapter);
        selectServiceCategoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putString("iVehicleCategoryId", "" + getSubCatagoryWithoutBannerList.get(position).get("iVehicleCategoryId"));
                bundle.putString("USER_PROFILE_JSON", "" + userProfileJson);
                bundle.putString("vCategoryName", "" + getSubCatagoryWithoutBannerList.get(position).get("vCategory"));
                new StartActProcess(getActContext()).startActWithData(UberXSelectServiceActivity.class, bundle);
            }
        });
    }

    private ArrayList getSubCatagoryData(String Id) {

        final ArrayList<HashMap<String, String>> subCatagoryList = new ArrayList<>();
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "getServiceCategories");
        parameters.put("parentId", Id);
        parameters.put("userId", generalFunc.getMemberId());

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(getActContext(), parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(CommonUtilities.action_str, responseString);

                    if (isDataAvail == true) {

                        JSONArray mainCataArray = generalFunc.getJsonArray("message", responseString);

                        for (int i = 0; i < mainCataArray.length(); i++) {
                            HashMap<String, String> map = new HashMap<String, String>();
                            JSONObject mainCataObject = generalFunc.getJsonObject(mainCataArray, i);
                            map.put("vCategory", generalFunc.getJsonValue("vCategory", mainCataObject.toString()));
                            map.put("vLogo_image", generalFunc.getJsonValue("vLogo_image", mainCataObject.toString()));
                            map.put("iVehicleCategoryId", generalFunc.getJsonValue("iVehicleCategoryId", mainCataObject.toString()));
                            subCatagoryList.add(map);
                        }
                        listCategoryAdapter.notifyDataSetChanged();
                    } else {
                        generalFunc.showGeneralMessage("", generalFunc.retrieveLangLBl("", generalFunc.getJsonValue("message", responseString)));
                    }

                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();

        return subCatagoryList;
    }

    private void intializeMainCatagory() {

        if (mainCatagoryrecyclerView.getVisibility() == View.GONE) {
            mainCatagoryrecyclerView.setVisibility(View.VISIBLE);
            subCatagoryListLayout.setVisibility(View.GONE);
        }
        final ViewGroup.LayoutParams params = mainCatagoryrecyclerView.getLayoutParams();
        params.height = ViewGroup.LayoutParams.MATCH_PARENT;
        mainCatagoryrecyclerView.setLayoutParams(params);

        GridLayoutManager mLayoutManager = new GridLayoutManager(getActContext(), 3);
        mainCatagoryrecyclerView.setLayoutManager(mLayoutManager);

        final ArrayList<HashMap<String, String>> mainCatagoryDataList = getMainCatagoryData(parentId);
        uberXMainCatagoryAdapter = new UberXMainCatagoryAdapter(getActContext(), mainCatagoryDataList);
        uberXMainCatagoryAdapter.setItemClickList(new UberXMainCatagoryAdapter.ItemClickListener() {
            @Override
            public void onClick(int position, int btnId) {

                subCatagoryLayout.setVisibility(View.VISIBLE);
                initializeSubCatagoryWithoutBanner(mainCatagoryDataList.get(position).get("iVehicleCategoryId"));
                addDrawer.setMenuState(false);

            }
        });
        mainCatagoryrecyclerView.setAdapter(uberXMainCatagoryAdapter);
    }

    private ArrayList getMainCatagoryData(String parentId) {

        final ArrayList<HashMap<String, String>> mainCatagoryList = new ArrayList<>();
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "getServiceCategories");
        parameters.put("parentId", "" + parentId);
        parameters.put("userId", generalFunc.getMemberId());

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(getActContext(), parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
//        exeWebServer.setIsDeviceTokenGenerate(true, "vDeviceToken");
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                closeLoader();
                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(CommonUtilities.action_str, responseString);

                    if (isDataAvail == true) {

                        JSONArray mainCataArray = generalFunc.getJsonArray("message", responseString);

                        for (int i = 0; i < mainCataArray.length(); i++) {
                            HashMap<String, String> map = new HashMap<String, String>();
                            JSONObject mainCataObject = generalFunc.getJsonObject(mainCataArray, i);
                            map.put("vCategory", generalFunc.getJsonValue("vCategory", mainCataObject.toString()));
                            map.put("vLogo_image", generalFunc.getJsonValue("vLogo_image", mainCataObject.toString()));
                            map.put("iVehicleCategoryId", generalFunc.getJsonValue("iVehicleCategoryId", mainCataObject.toString()));
                            mainCatagoryList.add(map);
                        }
                        uberXMainCatagoryAdapter.notifyDataSetChanged();
                    } else {
                        generalFunc.showGeneralMessage("", generalFunc.retrieveLangLBl("", generalFunc.getJsonValue("message", responseString)));
                    }
                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
        return mainCatagoryList;

    }

    private void setData() {
        headerLocAddressTxt.setHint(generalFunc.retrieveLangLBl("Enter Location...", "LBL_ENTER_LOC_HINT_TXT"));
        LocStaticTxt.setText(generalFunc.retrieveLangLBl("Location For availing Service", "LBL_LOCATION_FOR_AVAILING_TXT"));
    }

    public void getBanners() {
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "getBanners");
        parameters.put("iMemberId", generalFunc.getMemberId());

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(getActContext(), parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(CommonUtilities.action_str, responseString);

                    if (isDataAvail == true) {
                        JSONArray arr = generalFunc.getJsonArray(CommonUtilities.message_str, responseString);
                        ArrayList<String> imagesList = new ArrayList<String>();
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject obj_temp = generalFunc.getJsonObject(arr, i);
                            imagesList.add(generalFunc.getJsonValue("vImage", obj_temp.toString()));
                        }

                        intializeBannerSliding(imagesList);
                    }
                }
            }
        });
        exeWebServer.execute();
    }

    private void intializeBannerSliding(final ArrayList<String> listData) {


        UberXHomeActBannerAdapter xHomeActBannerAdapter = new UberXHomeActBannerAdapter(getActContext(), listData);
        ((ViewPager) findViewById(R.id.pager)).setAdapter(xHomeActBannerAdapter);

        LoopingCirclePageIndicator circlePageIndicator = new LoopingCirclePageIndicator(this);
        circlePageIndicator.setDataSize(listData.size());
        circlePageIndicator.setViewPager((ViewPager) findViewById(R.id.pager));
        mFrameLayout.addView(circlePageIndicator);
    }


    public void intializeSubCatagory() {

        if (subCatagoryListLayout.getVisibility() == View.GONE) {
            mainCatagoryrecyclerView.setVisibility(View.GONE);
            subCatagoryListLayout.setVisibility(View.VISIBLE);
        }

        (findViewById(R.id.uberXNoDataTxtView)).setVisibility(View.GONE);

        final LayoutInflater mInflater = (LayoutInflater)
                getActContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "getServiceCategories");
        parameters.put("parentId", "" + parentId);
        parameters.put("userId", generalFunc.getMemberId());

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(getActContext(), parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (uberXHomePageLoader.getVisibility() == View.VISIBLE) {
                    closeLoader();
                }
                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(CommonUtilities.action_str, responseString);


                    if (isDataAvail == true) {

                        JSONArray mainCataArray = generalFunc.getJsonArray("message", responseString);

                        for (int i = 0; i < mainCataArray.length(); i++) {

                            HashMap<String, String> map = new HashMap<String, String>();
                            final View view = mInflater.inflate(R.layout.item_sub_catagory_list_design, null);
                            final JSONObject mainCataObject = generalFunc.getJsonObject(mainCataArray, i);

                            final ImageView catagoryImageView = (ImageView) view.findViewById(R.id.catagoryImageView);
                            final ImageView catagoryArrowImageView = (ImageView) view.findViewById(R.id.arrowImageView);
                            MTextView catagoryTxt = (MTextView) view.findViewById(R.id.catagoryTxt);

                            catagoryTxt.setText(generalFunc.getJsonValue("vCategory", mainCataObject.toString()));

                            Picasso.with(getActContext())
                                    .load(generalFunc.getJsonValue("vLogo_image", mainCataObject.toString()))
                                    .into(catagoryImageView);

                            catagoryArrowImageView.setImageDrawable(getResources().getDrawable(R.mipmap.ic_arrow_down));

                            view.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Bundle bundle = new Bundle();
                                    bundle.putString("USER_PROFILE_JSON", userProfileJson);
                                    bundle.putString("iVehicleCategoryId", generalFunc.getJsonValue("iVehicleCategoryId", mainCataObject.toString()));
                                    bundle.putString("vCategoryName", generalFunc.getJsonValue("vCategory", mainCataObject.toString()));
                                    new StartActProcess(getActContext()).startActWithData(UberXSelectServiceActivity.class, bundle);
                                }
                            });

                            map.put("vCategory", generalFunc.getJsonValue("vCategory", mainCataObject.toString()));
                            subCatList.add(map);
                            subCatagoryListLayout.addView(view);
                        }
                    } else {

                        (findViewById(R.id.uberXNoDataTxtView)).setVisibility(View.VISIBLE);

                        generalFunc.showGeneralMessage("", generalFunc.retrieveLangLBl("", generalFunc.getJsonValue("message", responseString)));

                    }
                } else {

                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

    public void showLoader() {
        uberXHomePageLoader.setVisibility(View.VISIBLE);
    }

    public void closeLoader() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                uberXHomePageLoader.setVisibility(View.GONE);
            }
        }, 2000);
    }

    public Context getActContext() {
        return UberXHomeActivity.this;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Utils.MY_PROFILE_REQ_CODE && resultCode == RESULT_OK && data != null) {
            // String userProfileJson = data.getStringExtra("UserProfileJson");
            String userProfileJson = generalFunc.retrieveValue(CommonUtilities.USER_PROFILE_JSON);
            this.userProfileJson = userProfileJson;
            addDrawer.changeUserProfileJson(this.userProfileJson);
        } else if (requestCode == Utils.UBER_X_SEARCH_PICKUP_LOC_REQ_CODE && resultCode == RESULT_OK && data != null) {
            headerLocAddressTxt.setText(data.getStringExtra("Address"));
        } else if (requestCode == Utils.CARD_PAYMENT_REQ_CODE && resultCode == RESULT_OK && data != null) {
            //String userProfileJson = data.getStringExtra("UserProfileJson");
            String userProfileJson = generalFunc.retrieveValue(CommonUtilities.USER_PROFILE_JSON);
            this.userProfileJson = userProfileJson;
            addDrawer.changeUserProfileJson(this.userProfileJson);
        }
    }

    public void configCategoryView() {
        if (subCatagoryLayout.getVisibility() == View.VISIBLE) {
            subCatagoryLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {

        if (subCatagoryLayout.getVisibility() == View.VISIBLE) {
            configCategoryView();
            return;
        }

        super.onBackPressed();

    }

    @Override
    protected void onResume() {
        super.onResume();
        userProfileJson = generalFunc.retrieveValue(CommonUtilities.USER_PROFILE_JSON);
    }

    public class setOnClickLst implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Utils.hideKeyboard(getActContext());
            switch (v.getId()) {
                case R.id.headerLocAddressTxt:
                    Bundle bn = new Bundle();
                    bn.putString("isPickUpLoc", "true");
                    new StartActProcess(getActContext()).startActForResult(SearchPickupLocationActivity.class,
                            bn, Utils.UBER_X_SEARCH_PICKUP_LOC_REQ_CODE);
                    break;
            }
        }
    }
}
