package com.mobisquid.user;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.adapter.files.OngoingTripAdapter;
import com.general.files.ConfigPubNub;
import com.general.files.ExecuteWebServerUrl;
import com.general.files.GeneralFunctions;
import com.general.files.StartActProcess;
import com.utils.CommonUtilities;
import com.utils.Utils;
import com.view.ErrorView;
import com.view.MTextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Admin on 21-02-2017.
 */
public class OnGoingTripsActivity extends AppCompatActivity implements OngoingTripAdapter.OnItemClickListener {

    OngoingTripAdapter ongoingTripAdapter;
    String userProfileJson = "";
    String iTripId = "";
    String driverStatus = "";
    private MTextView titleTxt, noOngoingTripsTxt;
    private RecyclerView onGoingTripsListRecyclerView;
    private ImageView backImgView;
    private ProgressBar loading_ongoing_trips;
    private ErrorView errorView;
    private GeneralFunctions generalFunc;
    private ArrayList<HashMap<String, String>> list = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ongoingtrips_layout);


        init();
        setLables();
    }


    private void init() {
        noOngoingTripsTxt = (MTextView) findViewById(R.id.noOngoingTripsTxt);
        titleTxt = (MTextView) findViewById(R.id.titleTxt);
        backImgView = (ImageView) findViewById(R.id.backImgView);
        onGoingTripsListRecyclerView = (RecyclerView) findViewById(R.id.onGoingTripsListRecyclerView);
        loading_ongoing_trips = (ProgressBar) findViewById(R.id.loading_ongoing_trips);

        errorView = (ErrorView) findViewById(R.id.errorView);
        generalFunc = new GeneralFunctions(getActContext());
        //userProfileJson=getIntent().getStringExtra("UserProfileJson");
        userProfileJson = generalFunc.retrieveValue(CommonUtilities.USER_PROFILE_JSON);

        String Last_trip_data = generalFunc.getJsonValue("TripDetails", userProfileJson);
        iTripId = generalFunc.getJsonValue("iTripId", Last_trip_data);

//        getTripDeliveryLocations();


        backImgView.setOnClickListener(new setOnClickList());

    }

    @Override
    protected void onResume() {
        super.onResume();
        getOngoingUserTrips();

        ConfigPubNub.getInstance().unRegisterGcmReceiver();
        ConfigPubNub.getInstance().ConfigPubNub(getActContext());
        ConfigPubNub.getInstance().setTripId("", "");


    }


    private void setViews() {

        ongoingTripAdapter = new OngoingTripAdapter(getActContext(), list, generalFunc, false);
        onGoingTripsListRecyclerView.setAdapter(ongoingTripAdapter);
        ongoingTripAdapter.setOnItemClickListener(this);
        ongoingTripAdapter.notifyDataSetChanged();

        if (list.size() > 0) {
            onGoingTripsListRecyclerView.setVisibility(View.VISIBLE);
            noOngoingTripsTxt.setVisibility(View.GONE);
        } else {
            onGoingTripsListRecyclerView.setVisibility(View.GONE);
            noOngoingTripsTxt.setVisibility(View.VISIBLE);
        }
    }


    public void getOngoingUserTrips() {
        if (errorView.getVisibility() == View.VISIBLE) {
            errorView.setVisibility(View.GONE);
        }

        loading_ongoing_trips.setVisibility(View.VISIBLE);
        final HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "getOngoingUserTrips");
        parameters.put("iUserId", generalFunc.getMemberId());


        final ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(getActContext(), parameters);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {
                    list = new ArrayList<>();
                    closeLoader();

                    if (generalFunc.checkDataAvail(CommonUtilities.action_str, responseString) == true) {

                        JSONArray message = generalFunc.getJsonArray(CommonUtilities.message_str, responseString);

                        if (message != null && message.length() > 0) {
                            for (int i = 0; i < message.length(); i++) {
                                JSONObject jobject1 = generalFunc.getJsonObject(message, i);

                                HashMap<String, String> map = new HashMap<>();
                                map.put("iDriverId", generalFunc.getJsonValue("iDriverId", jobject1.toString()));
                                map.put("driverImage", generalFunc.getJsonValue("driverImage", jobject1.toString()));
                                map.put("driverName", generalFunc.getJsonValue("driverName", jobject1.toString()));
                                map.put("vCode", generalFunc.getJsonValue("vCode", jobject1.toString()));
                                map.put("driverMobile", generalFunc.getJsonValue("driverMobile", jobject1.toString()));
                                map.put("driverStatus", generalFunc.getJsonValue("driverStatus", jobject1.toString()));
                                map.put("driverRating", generalFunc.getJsonValue("driverRating", jobject1.toString()));
                                map.put("vRideNo", generalFunc.getJsonValue("vRideNo", jobject1.toString()));
                                map.put("tSaddress", generalFunc.getJsonValue("tSaddress", jobject1.toString()));
                                map.put("iTripId", generalFunc.getJsonValue("iTripId", jobject1.toString()));
                                map.put("senderName", generalFunc.getJsonValue("vName", userProfileJson) + " " + generalFunc.getJsonValue("vLastName", userProfileJson));
                                map.put("Booking_LBL", generalFunc.retrieveLangLBl("Booking No", "LBL_BOOKING"));
                                map.put("dDateOrig", generalFunc.getJsonValue("dDateOrig", jobject1.toString()));
                                map.put("SelectedTypeName", generalFunc.getJsonValue("SelectedTypeName", jobject1.toString()));
                                driverStatus = generalFunc.getJsonValue("driverStatus", jobject1.toString());
                                map.put("driverLatitude", generalFunc.getJsonValue("driverLatitude", jobject1.toString()));
                                map.put("driverLongitude", generalFunc.getJsonValue("driverLongitude", jobject1.toString()));

                                map.put("tStratLat", generalFunc.getJsonValue("driverLatitude", jobject1.toString()));
                                map.put("tStartLong", generalFunc.getJsonValue("driverLongitude", jobject1.toString()));
                                map.put("eFareType", generalFunc.getJsonValue("eFareType", jobject1.toString()));
                                list.add(map);

                            }
                        }


                    }


                } else {
                    generateErrorView();
                }
                setViews();

            }
        });
        exeWebServer.execute();
    }

    public void closeLoader() {
        if (loading_ongoing_trips.getVisibility() == View.VISIBLE) {
            loading_ongoing_trips.setVisibility(View.GONE);
        }
    }

    public void generateErrorView() {

        closeLoader();

        generalFunc.generateErrorView(errorView, "LBL_ERROR_TXT", "LBL_NO_INTERNET_TXT");

        if (errorView.getVisibility() != View.VISIBLE) {
            errorView.setVisibility(View.VISIBLE);
            noOngoingTripsTxt.setVisibility(View.GONE);
        }
        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                getOngoingUserTrips();
            }
        });
    }


    @Override
    public void onItemClickList(String type, int position) {
        Utils.hideKeyboard(getActContext());
        if (type.equalsIgnoreCase("View Detail")) {
            Bundle bn = new Bundle();
            bn.putSerializable("TripDetail", list.get(position));
            bn.putSerializable("driverStatus", driverStatus);

            new StartActProcess(getActContext()).startActForResult(OnGoingTripDetailsActivity.class, bn, Utils.LIVE_TRACK_REQUEST_CODE);
        } else if (type.equalsIgnoreCase("Live Track")) {

            generalFunc.autoLogin(OnGoingTripsActivity.this, list.get(position).get("iTripId"));

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Utils.LIVE_TRACK_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
        }
    }

    private void setLables() {


        titleTxt.setText(generalFunc.retrieveLangLBl("My Ongoing Trips", "LBL_MY_ON_GOING_JOB"));
        noOngoingTripsTxt.setText(generalFunc.retrieveLangLBl("No Ongoing Trips Available.", "LBL_NO_ONGOING_TRIPS_AVAIL"));
    }

    private Activity getActContext() {
        return OnGoingTripsActivity.this;
    }

    @Override
    public void onBackPressed() {

        //bug_004 start
        if (generalFunc.getJsonValue("APP_TYPE", userProfileJson).equalsIgnoreCase(Utils.CabGeneralTypeRide_Delivery_UberX)) {


            if ((generalFunc.getJsonValue("vTripStatus", userProfileJson).equalsIgnoreCase("Active") ||
                    generalFunc.getJsonValue("vTripStatus", userProfileJson).equalsIgnoreCase("On Going Trip")) && !generalFunc.getJsonValue("eType", userProfileJson).equalsIgnoreCase(Utils.CabGeneralType_UberX)) {

                OnGoingTripsActivity.super.onBackPressed();
            } else if (getIntent().getBooleanExtra("isRestart", true)) {
                Bundle bn = new Bundle();
                new StartActProcess(getActContext()).startActWithData(UberXActivity.class, bn);
                finishAffinity();
            } else {
                OnGoingTripsActivity.super.onBackPressed();
            }
        } else {
            OnGoingTripsActivity.super.onBackPressed();
        }
        //bug_004 end

    }

    public class setOnClickList implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            Utils.hideKeyboard(getActContext());
            if (view.getId() == R.id.backImgView) {

                onBackPressed();

            }
        }
    }
}
