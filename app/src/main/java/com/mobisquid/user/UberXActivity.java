package com.mobisquid.user;

import android.animation.LayoutTransition;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.adapter.files.UberXBannerPagerAdapter;
import com.adapter.files.UberXCategoryAdapter;
import com.dialogs.NoInternetConnectionDialog;
import com.general.files.AddDrawer;
import com.general.files.ConfigPubNub;
import com.general.files.DividerItemDecoration;
import com.general.files.ExecuteWebServerUrl;
import com.general.files.GeneralFunctions;
import com.general.files.GetAddressFromLocation;
import com.general.files.GetLocationUpdates;
import com.general.files.InternetConnection;
import com.general.files.StartActProcess;
import com.general.files.UpdateFrequentTask;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.utils.CommonUtilities;
import com.utils.Utils;
import com.view.LoopingCirclePageIndicator;
import com.view.MTextView;
import com.view.SelectableRoundedImageView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class UberXActivity extends AppCompatActivity implements UberXCategoryAdapter.OnItemClickList, UpdateFrequentTask.OnTaskRunCalled, ViewPager.OnPageChangeListener
        , GetAddressFromLocation.AddressFound, GetLocationUpdates.LocationUpdates {

    public String userProfileJson = "";
    public LinearLayout noloactionview;
    public ImageView nolocmenuImgView;
    public ImageView nolocbackImgView;
    public JSONObject obj_userProfile;
    int BANNER_AUTO_ROTATE_INTERVAL = 4000;
    RecyclerView dataListRecyclerView;
    View bannerArea;

    ViewPager bannerViewPager;
    LoopingCirclePageIndicator bannerCirclePageIndicator;
    UberXBannerPagerAdapter bannerAdapter;
    GeneralFunctions generalFunc;
    AddDrawer addDrawer;
    MTextView headerLocAddressTxt, LocStaticTxt, selectServiceTxt;
    HashMap<String, String> generalCategoryIconTypeDataMap = new HashMap<String, String>();
    ArrayList<HashMap<String, String>> generalCategoryList = new ArrayList<>();

    ArrayList<HashMap<String, String>> mainCategoryList = new ArrayList<>();
    ArrayList<HashMap<String, String>> subCategoryList = new ArrayList<>();

    ArrayList<HashMap<String, String>> allMainCategoryList = new ArrayList<>();

    UberXCategoryAdapter ufxCatAdapter;
    String CAT_TYPE_MODE = "0";
    String latitude = "0.0";
    String longitude = "0.0";
    UpdateFrequentTask updateBannerChangeFreqTask;
    int currentBannerPosition = 0;
    ImageView backImgView, menuImgView;
    String address;
    GetLocationUpdates getLastLocation;
    boolean isUfxaddress = false;
    GetAddressFromLocation getAddressFromLocation;
    boolean isgpsview = false;
    MTextView noLocTitleTxt, noLocMsgTxt, settingTxt, pickupredirectTxt;
    ConfigPubNub configPubNub;
    boolean isArrivedPopup = false;
    boolean isstartPopup = false;
    boolean isEndpopup = false;
    boolean isCancelpopup = false;
    boolean isback = false;
    View rduTopArea;
    LinearLayout ongoingJobArea, historyArea, walletArea, profileArea;
    MTextView ongoingjobTxt, historyTxt, walletTxt, profileTxt;

    ImageView headerLogo;
    LinearLayout uberXHeaderLayout;

    ImageView noLocImgView;


    private NoInternetConnectionDialog noInternetConn;

    InternetConnection intCheck;
    ImageView myjob_img;

    DividerItemDecoration itemDecoration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uber_x);

        generalFunc = new GeneralFunctions(getActContext());

        isUfxaddress = false;


        userProfileJson = generalFunc.retrieveValue(CommonUtilities.USER_PROFILE_JSON);
        // userProfileJson = getIntent().getStringExtra("USER_PROFILE_JSON");

        itemDecoration = new DividerItemDecoration(getActContext(), DividerItemDecoration.VERTICAL_LIST);

        getLastLocation = new GetLocationUpdates(getActContext(), Utils.LOCATION_UPDATE_MIN_DISTANCE_IN_MITERS, false, this);
        intCheck = new InternetConnection(getActContext());
        try {
            address = getIntent().getStringExtra("uberXAddress");
        } catch (Exception e) {
        }

        latitude = getIntent().getDoubleExtra("uberXlat", 0.0) + "";
        longitude = getIntent().getDoubleExtra("uberXlong", 0.0) + "";

        isback = getIntent().getBooleanExtra("isback", false);

//        seperationLine = findViewById(R.id.seperationLine);

        rduTopArea = findViewById(R.id.rduTopArea);
        ongoingJobArea = findViewById(R.id.ongoingJobArea);
        historyArea = findViewById(R.id.historyArea);
        walletArea = findViewById(R.id.walletArea);
        profileArea = findViewById(R.id.profileArea);
//        rduNestedScrollView = findViewById(R.id.rduNestedScrollView);
        myjob_img = (ImageView) findViewById(R.id.myjob_img);

        ongoingJobArea.setOnClickListener(new setOnClickList());
        historyArea.setOnClickListener(new setOnClickList());
        walletArea.setOnClickListener(new setOnClickList());
        profileArea.setOnClickListener(new setOnClickList());
        ongoingjobTxt = findViewById(R.id.ongoingjobTxt);
        historyTxt = findViewById(R.id.historyTxt);
        walletTxt = findViewById(R.id.walletTxt);
        profileTxt = findViewById(R.id.profileTxt);
        headerLogo = findViewById(R.id.headerLogo);
        uberXHeaderLayout = findViewById(R.id.uberXHeaderLayout);
        selectServiceTxt = (MTextView) findViewById(R.id.selectServiceTxt);
        noLocImgView = (ImageView) findViewById(R.id.noLocImgView);

//        bannerListRecyclerView = (RecyclerView) findViewById(R.id.bannerListRecyclerView);


        profileTxt.setText(generalFunc.retrieveLangLBl("", "LBL_HEADER_RDU_PROFILE"));
        walletTxt.setText(generalFunc.retrieveLangLBl("", "LBL_HEADER_RDU_WALLET"));
        historyTxt.setText(generalFunc.retrieveLangLBl("", "LBL_HEADER_RDU_BOOKINGS"));
        ongoingjobTxt.setText(generalFunc.retrieveLangLBl("", "LBL_HEADER_RDU_ACTIVE_JOBS"));


        dataListRecyclerView = (RecyclerView) findViewById(R.id.dataListRecyclerView);
        bannerArea = findViewById(R.id.bannerArea);
        bannerViewPager = (ViewPager) findViewById(R.id.bannerViewPager);


        if (generalFunc.getJsonValue("APP_TYPE", userProfileJson).equalsIgnoreCase(Utils.CabGeneralTypeRide_Delivery_UberX)) {
            bannerArea.setVisibility(View.GONE);
            rduTopArea.setVisibility(View.VISIBLE);
            headerLogo.setVisibility(View.VISIBLE);
            uberXHeaderLayout.setVisibility(View.GONE);
            selectServiceTxt.setVisibility(View.GONE);
//            bannerListRecyclerView.setVisibility(View.VISIBLE);


        } else {
            bannerArea.setVisibility(View.VISIBLE);
            rduTopArea.setVisibility(View.GONE);
            headerLogo.setVisibility(View.GONE);
            uberXHeaderLayout.setVisibility(View.VISIBLE);
            selectServiceTxt.setVisibility(View.VISIBLE);

            // Set BannerArea Height as per aspect ratio
            CollapsingToolbarLayout.LayoutParams lyParamsBannerArea = (CollapsingToolbarLayout.LayoutParams) bannerArea.getLayoutParams();
            lyParamsBannerArea.height = Utils.getHeightOfBanner(getActContext(), 0);
            bannerArea.setLayoutParams(lyParamsBannerArea);
        }


        bannerCirclePageIndicator = (LoopingCirclePageIndicator) findViewById(R.id.bannerCirclePageIndicator);
        headerLocAddressTxt = (MTextView) findViewById(R.id.headerLocAddressTxt);
        LocStaticTxt = (MTextView) findViewById(R.id.LocStaticTxt);

        backImgView = (ImageView) findViewById(R.id.backImgView);
        menuImgView = (ImageView) findViewById(R.id.menuImgView);
        backImgView.setOnClickListener(new setOnClickLst());


        noloactionview = (LinearLayout) findViewById(R.id.noloactionview);
        noLocTitleTxt = (MTextView) findViewById(R.id.noLocTitleTxt);
        noLocMsgTxt = (MTextView) findViewById(R.id.noLocMsgTxt);
        settingTxt = (MTextView) findViewById(R.id.settingTxt);
        pickupredirectTxt = (MTextView) findViewById(R.id.pickupredirectTxt);

        nolocmenuImgView = (ImageView) findViewById(R.id.nolocmenuImgView);
        nolocbackImgView = (ImageView) findViewById(R.id.nolocbackImgView);


        nolocmenuImgView.setOnClickListener(new setOnClickList());
        nolocbackImgView.setOnClickListener(new setOnClickList());
        settingTxt.setOnClickListener(new setOnClickList());
        pickupredirectTxt.setOnClickListener(new setOnClickList());


        if (isback) {
            menuImgView.setVisibility(View.GONE);
            backImgView.setVisibility(View.VISIBLE);

        }
        addDrawer = new AddDrawer(getActContext(), userProfileJson, isback);

        /*if (generalFunc.getJsonValue("APP_TYPE", userProfileJson).equalsIgnoreCase(Utils.CabGeneralTypeRide_Delivery_UberX)) {
            ufxCatAdapter = new UberXCategoryAdapter(getActContext(), generalCategoryListRDU, generalFunc);
        } else {*/
        ufxCatAdapter = new UberXCategoryAdapter(getActContext(), generalCategoryList, generalFunc);
        /*}*/

        if (generalFunc.getJsonValue(CommonUtilities.UBERX_PARENT_CAT_ID, userProfileJson).equalsIgnoreCase("0")) {
            /*dataListRecyclerView.setLayoutManager(new GridLayoutManager(getActContext(), getNumOfColumns(), GridLayoutManager.VERTICAL, false) {
                @Override
                public boolean canScrollHorizontally() {
                    return false;
                }

                @Override
                public boolean canScrollVertically() {
                    return false;
                }
            });*/
            CAT_TYPE_MODE = "0";
            setParentCategoryLayoutManager();

        } else {
//            dataListRecyclerView.setLayoutManager(new LinearLayoutManager(getActContext()));
            /*dataListRecyclerView.setLayoutManager(new LinearLayoutManager(getActContext(), LinearLayoutManager.VERTICAL, false) {
                @Override
                public boolean canScrollHorizontally() {
                    return false;
                }

                @Override
                public boolean canScrollVertically() {
                    return false;
                }
            });*/
            dataListRecyclerView.setLayoutManager(new LinearLayoutManager(getActContext()));

            CAT_TYPE_MODE = "1";
        }
        dataListRecyclerView.setAdapter(ufxCatAdapter);

        uberXHeaderLayout.setOnClickListener(new setOnClickLst());

        if (addDrawer != null) {

            addDrawer.setItemClickList(() -> configCategoryView());
        }

        setData();

        if (!generalFunc.getJsonValue("APP_TYPE", userProfileJson).equalsIgnoreCase(Utils.CabGeneralTypeRide_Delivery_UberX)) {
            getBanners();
        }

        Utils.printLog("UFXCategoryId", "::" + generalFunc.getJsonValue(CommonUtilities.UBERX_PARENT_CAT_ID, userProfileJson));
        getCategory(generalFunc.getJsonValue(CommonUtilities.UBERX_PARENT_CAT_ID, userProfileJson), CAT_TYPE_MODE);


        bannerViewPager.addOnPageChangeListener(this);


    }

    private void setParentCategoryLayoutManager() {
        GridLayoutManager gridLay = new GridLayoutManager(getActContext(), getNumOfColumns());
        gridLay.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
//                HashMap<String, String> dataMap = generalCategoryList.get(position);
//                if (dataMap.get("eShowType") != null && !dataMap.get("eShowType").equalsIgnoreCase("ICON")) {
//                    return getNumOfColumns();
//                }
//                Utils.printLog("GridSetting","::"+generalCategoryIconTypeDataMap.get(position));
                if (generalCategoryIconTypeDataMap.get("" + position) != null && !generalCategoryIconTypeDataMap.get("" + position).equalsIgnoreCase("ICON")) {
                    return getNumOfColumns();
                }
                return 1;
            }
        });
        dataListRecyclerView.setLayoutManager(gridLay);
    }


    public void NoLocationView() {

        if (!generalFunc.isLocationEnabled()) {
            noloactionview.setVisibility(View.VISIBLE);

        } else {
            if (getLastLocation != null) {
                getLastLocation.stopLocationUpdates();
                getLastLocation = null;
            }
            getLastLocation = new GetLocationUpdates(getActContext(), Utils.LOCATION_UPDATE_MIN_DISTANCE_IN_MITERS, false, this);

            noloactionview.setVisibility(View.GONE);
        }


    }

    private void setData() {
        headerLocAddressTxt.setHint(generalFunc.retrieveLangLBl("Enter Location...", "LBL_ENTER_LOC_HINT_TXT"));
        LocStaticTxt.setText(generalFunc.retrieveLangLBl("Location For availing Service", "LBL_LOCATION_FOR_AVAILING_TXT"));
        selectServiceTxt.setText(generalFunc.retrieveLangLBl("Select Service", "LBL_SELECT_SERVICE_TXT"));

        noLocTitleTxt.setText(generalFunc.retrieveLangLBl("", "LBL_LOCATION_SERVICES_TURNED_OFF"));
        noLocMsgTxt.setText(generalFunc.retrieveLangLBl("", "LBL_LOCATION_SERVICES_TURNED_OFF_DETAILS"));
        settingTxt.setText(generalFunc.retrieveLangLBl("", "LBL_TURN_ON_LOC_SERVICE"));
        pickupredirectTxt.setText(generalFunc.retrieveLangLBl("Enter source address", "LBL_ENTER_SOURCE_TXT"));

        if (isback) {
            if (getIntent().getStringExtra("address") != null && !getIntent().getStringExtra("address").equalsIgnoreCase("")) {
                headerLocAddressTxt.setText(getIntent().getStringExtra("address"));
                latitude = getIntent().getStringExtra("lat");
                longitude = getIntent().getStringExtra("long");

            }
        }


    }


    public void getBanners() {
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "getBanners");
        parameters.put("iMemberId", generalFunc.getMemberId());
        parameters.put("WidthOfBanner", "" + Utils.getWidthOfBanner(getActContext(), 0));
        parameters.put("HeightOfBanner", "" + Utils.getHeightOfBanner(getActContext(), 0));

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(getActContext(), parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(responseString -> {

            if (responseString != null && !responseString.equals("")) {

                boolean isDataAvail = GeneralFunctions.checkDataAvail(CommonUtilities.action_str, responseString);

                if (isDataAvail == true) {
                    JSONArray arr = generalFunc.getJsonArray(CommonUtilities.message_str, responseString);
                    ArrayList<String> imagesList = new ArrayList<String>();
                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject obj_temp = generalFunc.getJsonObject(arr, i);
                        imagesList.add(generalFunc.getJsonValue("vImage", obj_temp.toString()));
                    }

                    if (imagesList.size() > 2) {
                        bannerViewPager.setOffscreenPageLimit(3);
                    } else if (imagesList.size() > 1) {
                        bannerViewPager.setOffscreenPageLimit(2);
                    }

                    UberXBannerPagerAdapter bannerAdapter = new UberXBannerPagerAdapter(getActContext(), imagesList);
                    bannerViewPager.setAdapter(bannerAdapter);

                    UberXActivity.this.bannerAdapter = bannerAdapter;

                    bannerCirclePageIndicator.setDataSize(imagesList.size());
                    bannerCirclePageIndicator.setViewPager(bannerViewPager);

                    if (imagesList.size() > 1) {
                        updateBannerChangeFreqTask = new UpdateFrequentTask(BANNER_AUTO_ROTATE_INTERVAL);
                        updateBannerChangeFreqTask.setTaskRunListener(UberXActivity.this);
                        updateBannerChangeFreqTask.avoidFirstRun();
                        updateBannerChangeFreqTask.startRepeatingTask();
                    }
                }
            } else {
//                    generalFunc.showError();
            }
        });
        exeWebServer.execute();
    }

    public Context getActContext() {
        return UberXActivity.this;
    }

    public void getCategory(String parentId, final String CAT_TYPE_MODE) {
        generalCategoryList.clear();
//        generalCategoryListRDU.clear();
        if (!CAT_TYPE_MODE.equals("0")) {
            subCategoryList.clear();
//            subCategoryListRDU.clear();
        }
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "getServiceCategories");
        parameters.put("parentId", "" + parentId);
        parameters.put("userId", generalFunc.getMemberId());
        parameters.put("WidthHeightOfGrid", "" + getActContext().getResources().getDimensionPixelSize(R.dimen.category_grid_size));
        parameters.put("WidthOfBanner", "" + Utils.getWidthOfBanner(getActContext(), getResources().getDimensionPixelSize(R.dimen.category_banner_left_right_margin) * 2));
        parameters.put("HeightOfBanner", "" + Utils.getHeightOfBanner(getActContext(), getResources().getDimensionPixelSize(R.dimen.category_banner_left_right_margin) * 2));

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(getActContext(), parameters);
        exeWebServer.setCancelAble(false);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(responseString -> {

            if (responseString != null && !responseString.equals("")) {

                boolean isDataAvail = GeneralFunctions.checkDataAvail(CommonUtilities.action_str, responseString);

                //RIDE_DELIVERY_SHOW_SELECTION

                if (isDataAvail == true) {
                    int GRID_TILES_MAX_COUNT = GeneralFunctions.parseIntegerValue(1, generalFunc.getJsonValueStr("GRID_TILES_MAX_COUNT", obj_userProfile));

                    JSONArray mainCataArray = generalFunc.getJsonArray("message", responseString);

                    ArrayList<HashMap<String, String>> bannerList = new ArrayList<>();
                    ArrayList<HashMap<String, String>> moreItemList = new ArrayList<>();

                    int gridCount = 0;

                    for (int i = 0; i < mainCataArray.length(); i++) {
                        HashMap<String, String> map = new HashMap<String, String>();
                        JSONObject categoryObj = generalFunc.getJsonObject(mainCataArray, i);
                        map.put("vCategory", generalFunc.getJsonValueStr("vCategory", categoryObj));
                        map.put("vLogo_image", generalFunc.getJsonValueStr("vLogo_image", categoryObj));
                        map.put("iVehicleCategoryId", generalFunc.getJsonValueStr("iVehicleCategoryId", categoryObj));
                        map.put("vCategoryBanner", generalFunc.getJsonValueStr("vCategoryBanner", categoryObj));
                        map.put("vBannerImage", generalFunc.getJsonValueStr("vBannerImage", categoryObj));
                        map.put("LBL_BOOK_NOW", generalFunc.retrieveLangLBl("", "LBL_BOOK_NOW"));

                        if (CAT_TYPE_MODE.equals("0")) {
                            allMainCategoryList.add(map);
                            if (generalFunc.getJsonValueStr("APP_TYPE", obj_userProfile).equalsIgnoreCase(Utils.CabGeneralTypeRide_Delivery_UberX)) {
                                map.put("eShowType", generalFunc.getJsonValueStr("eShowType", categoryObj));


                                String eShowType = generalFunc.getJsonValueStr("eShowType", categoryObj);
                                if (eShowType.equalsIgnoreCase("ICON")) {
                                    if (gridCount < GRID_TILES_MAX_COUNT) {
                                        mainCategoryList.add(map);
                                    } else {
                                        moreItemList.add(map);
                                    }
                                    gridCount = gridCount + 1;
                                } else {
                                    bannerList.add(map);
                                }
                            } else {
                                map.put("eShowType", "Icon");

                                mainCategoryList.add(map);
                            }
                        } else {
                            subCategoryList.add(map);
                        }
                    }


                    if (CAT_TYPE_MODE.equals("0")) {
                        if (generalFunc.getJsonValueStr("APP_TYPE", obj_userProfile).equalsIgnoreCase(Utils.CabGeneralTypeRide_Delivery_UberX)) {
                            ArrayList<HashMap<String, String>> newBannerList = setRDUCategories(bannerList);

                            if (moreItemList.size() > 0) {
                                HashMap<String, String> mapDataMore = new HashMap<>();
                                mapDataMore.put("eCatType", "More");
                                mapDataMore.put("eShowType", "Icon");
                                mapDataMore.put("vCategory", generalFunc.retrieveLangLBl("", "LBL_MORE"));
                                mapDataMore.put("vLogo_image", "" + R.mipmap.ic_more);
                                mainCategoryList.add(mapDataMore);

                                if (mainCategoryList.size() % getNumOfColumns() != 0) {
                                    mainCategoryList.remove(mainCategoryList.size() - 1);

                                    int totCount = 0;
                                    while ((mainCategoryList.size() + 1) % getNumOfColumns() != 0) {
                                        mainCategoryList.add(moreItemList.get(totCount));
                                        totCount = totCount + 1;

                                        if (totCount >= mainCategoryList.size()) {
                                            break;
                                        }
                                    }
                                    mainCategoryList.add(mapDataMore);
                                }
                            }

                            mainCategoryList.addAll(newBannerList);
                        }

                        generalCategoryList.addAll(mainCategoryList);

                        if (generalFunc.getJsonValueStr("APP_TYPE", obj_userProfile).equalsIgnoreCase(Utils.CabGeneralTypeRide_Delivery_UberX)) {
                            for (int i = 0; i < generalCategoryList.size(); i++) {
                                generalCategoryIconTypeDataMap.put("" + i, "" + (generalCategoryList.get(i).get("eShowType") == null ? "" : generalCategoryList.get(i).get("eShowType")));
                            }
                        }

                    } else {
                        generalCategoryList.addAll(subCategoryList);
                    }


                    ufxCatAdapter = null;
                    ufxCatAdapter = new UberXCategoryAdapter(getActContext(), generalCategoryList, generalFunc);

                    if (!CAT_TYPE_MODE.equalsIgnoreCase("0")) {
                        dataListRecyclerView.addItemDecoration(itemDecoration);
                    } else {
                        dataListRecyclerView.removeItemDecoration(itemDecoration);
                    }

                    ufxCatAdapter.setCategoryMode(CAT_TYPE_MODE);
                    dataListRecyclerView.setAdapter(ufxCatAdapter);
                    ufxCatAdapter.notifyDataSetChanged();
                    ufxCatAdapter.setOnItemClickList(UberXActivity.this);

                } else {
                    generalFunc.showGeneralMessage("", generalFunc.retrieveLangLBl("", generalFunc.getJsonValue("message", responseString)));
                }

            } else {
                generalFunc.showError();
            }
        });
        exeWebServer.execute();
    }

    private ArrayList<HashMap<String, String>> setRDUCategories(ArrayList<HashMap<String, String>> bannerList) {
        ArrayList<HashMap<String, String>> newBannerList = new ArrayList<>();

        ArrayList<HashMap<String, String>> gridIconsList = new ArrayList<>();
        ArrayList<HashMap<String, String>> generalIconsList = new ArrayList<>();

        if (generalFunc.getJsonValueStr("APP_TYPE", obj_userProfile).equalsIgnoreCase(Utils.CabGeneralTypeRide_Delivery_UberX)) {

            HashMap<String, String> rideData = new HashMap<>();
            rideData.put("eCatType", "Ride");
            rideData.put("vCategory", generalFunc.retrieveLangLBl("", "LBL_HEADER_RDU_RIDE"));
            rideData.put("vCategoryBanner", generalFunc.retrieveLangLBl("", "LBL_HEADER_RDU_ON_DEMAND_RIDE"));
            rideData.put("vLogo_image", generalFunc.getJsonValueStr("RIDE_GRID_ICON_NAME", obj_userProfile));
            rideData.put("vBannerImage", generalFunc.getJsonValueStr("RIDE_BANNER_IMG_NAME", obj_userProfile));
            rideData.put("LBL_BOOK_NOW", generalFunc.retrieveLangLBl("", "LBL_BOOK_NOW"));

            HashMap<String, String> motoRideData = new HashMap<>();
            motoRideData.put("eCatType", "MotoRide");
            motoRideData.put("vCategory", generalFunc.retrieveLangLBl("", "LBL_HEADER_RDU_MOTO_RIDE"));
            motoRideData.put("vCategoryBanner", generalFunc.retrieveLangLBl("", "LBL_HEADER_RDU_ON_DEMAND_MOTO_RIDE"));
            motoRideData.put("vLogo_image", generalFunc.getJsonValueStr("MOTO_RIDE_GRID_ICON_NAME", obj_userProfile));
            motoRideData.put("vBannerImage", generalFunc.getJsonValueStr("MOTO_RIDE_BANNER_IMG_NAME", obj_userProfile));
            motoRideData.put("LBL_BOOK_NOW", generalFunc.retrieveLangLBl("", "LBL_BOOK_NOW"));

            HashMap<String, String> deliveryData = new HashMap<>();
            deliveryData.put("eCatType", "Delivery");
            deliveryData.put("vCategory", generalFunc.retrieveLangLBl("", "LBL_HEADER_RDU_DELIVERY"));
            deliveryData.put("vCategoryBanner", generalFunc.retrieveLangLBl("", "LBL_HEADER_RDU_ON_DEMAND_DELIVERY"));
            deliveryData.put("vLogo_image", generalFunc.getJsonValueStr("DELIVERY_GRID_ICON_NAME", obj_userProfile));
            deliveryData.put("vBannerImage", generalFunc.getJsonValueStr("DELIVERY_BANNER_IMG_NAME", obj_userProfile));
            deliveryData.put("LBL_BOOK_NOW", generalFunc.retrieveLangLBl("", "LBL_BOOK_NOW"));

            HashMap<String, String> motoDeliveryData = new HashMap<>();
            motoDeliveryData.put("eCatType", "MotoDelivery");
            motoDeliveryData.put("vCategory", generalFunc.retrieveLangLBl("", "LBL_HEADER_RDU_MOTO_DELIVERY"));
            motoDeliveryData.put("vCategoryBanner", generalFunc.retrieveLangLBl("", "LBL_HEADER_RDU_ON_DEMAND_MOTO_DELIVERY"));
            motoDeliveryData.put("vLogo_image", generalFunc.getJsonValueStr("MOTO_DELIVERY_GRID_ICON_NAME", obj_userProfile));
            motoDeliveryData.put("vBannerImage", generalFunc.getJsonValueStr("MOTO_DELIVERY_BANNER_IMG_NAME", obj_userProfile));
            motoDeliveryData.put("LBL_BOOK_NOW", generalFunc.retrieveLangLBl("", "LBL_BOOK_NOW"));



            String RIDE_SHOW_SELECTION = generalFunc.getJsonValueStr("RIDE_SHOW_SELECTION", obj_userProfile);
            String MOTO_RIDE_SHOW_SELECTION = generalFunc.getJsonValueStr("MOTO_RIDE_SHOW_SELECTION", obj_userProfile);
            String DELIVERY_SHOW_SELECTION = generalFunc.getJsonValueStr("DELIVERY_SHOW_SELECTION", obj_userProfile);
            String MOTO_DELIVERY_SHOW_SELECTION = generalFunc.getJsonValueStr("MOTO_DELIVERY_SHOW_SELECTION", obj_userProfile);

            if (RIDE_SHOW_SELECTION.equalsIgnoreCase("ICON")) {
                rideData.put("eShowType", "Icon");
                generalIconsList.add(rideData);
                gridIconsList.add(rideData);
            } else if (RIDE_SHOW_SELECTION.equalsIgnoreCase("ICON-BANNER")) {
                rideData.put("eShowType", "Icon");
                generalIconsList.add(new HashMap<>(rideData));
                gridIconsList.add(new HashMap<>(rideData));
                rideData.put("eShowType", "Banner");
                newBannerList.add(new HashMap<>(rideData));
            } else if (RIDE_SHOW_SELECTION.equalsIgnoreCase("BANNER")) {
                rideData.put("eShowType", "Banner");
                generalIconsList.add(new HashMap<>(rideData));
                newBannerList.add(rideData);
            }

            if (MOTO_RIDE_SHOW_SELECTION.equalsIgnoreCase("ICON")) {
                motoRideData.put("eShowType", "Icon");
                generalIconsList.add(motoRideData);
                gridIconsList.add(motoRideData);
            } else if (MOTO_RIDE_SHOW_SELECTION.equalsIgnoreCase("ICON-BANNER")) {
                motoRideData.put("eShowType", "Icon");
                generalIconsList.add(new HashMap<>(motoRideData));
                gridIconsList.add(new HashMap<>(motoRideData));

                motoRideData.put("eShowType", "Banner");
                newBannerList.add(new HashMap<>(motoRideData));
            } else if (MOTO_RIDE_SHOW_SELECTION.equalsIgnoreCase("BANNER")) {
                motoRideData.put("eShowType", "Banner");
                generalIconsList.add(new HashMap<>(motoRideData));
                newBannerList.add(motoRideData);
            }



            if (DELIVERY_SHOW_SELECTION.equalsIgnoreCase("ICON")) {
                deliveryData.put("eShowType", "Icon");
                generalIconsList.add(deliveryData);
                gridIconsList.add(deliveryData);
            } else if (DELIVERY_SHOW_SELECTION.equalsIgnoreCase("ICON-BANNER")) {
                deliveryData.put("eShowType", "Icon");
                generalIconsList.add(new HashMap<>(deliveryData));
                gridIconsList.add(new HashMap<>(deliveryData));
                deliveryData.put("eShowType", "Banner");
                newBannerList.add(new HashMap<>(deliveryData));
            } else if (DELIVERY_SHOW_SELECTION.equalsIgnoreCase("BANNER")) {
                deliveryData.put("eShowType", "Banner");
                generalIconsList.add(new HashMap<>(deliveryData));
                newBannerList.add(deliveryData);
            }

            if (MOTO_DELIVERY_SHOW_SELECTION.equalsIgnoreCase("ICON")) {
                motoDeliveryData.put("eShowType", "Icon");
                generalIconsList.add(motoDeliveryData);
                gridIconsList.add(motoDeliveryData);
            } else if (MOTO_DELIVERY_SHOW_SELECTION.equalsIgnoreCase("ICON-BANNER")) {
                motoDeliveryData.put("eShowType", "Icon");
                generalIconsList.add(new HashMap<>(motoDeliveryData));
                gridIconsList.add(new HashMap<>(motoDeliveryData));
                motoDeliveryData.put("eShowType", "Banner");
                newBannerList.add(new HashMap<>(motoDeliveryData));
            } else if (MOTO_DELIVERY_SHOW_SELECTION.equalsIgnoreCase("BANNER")) {
                motoDeliveryData.put("eShowType", "Banner");
                generalIconsList.add(new HashMap<>(motoDeliveryData));
                newBannerList.add(motoDeliveryData);
            }


        }
        newBannerList.addAll(bannerList);

//        if(isFromWebTask == false){
//            generalIconsList
//            generalCategoryList.addAll(generalIconsList);
//        }else{

        generalIconsList.addAll(allMainCategoryList);
        allMainCategoryList.clear();


        allMainCategoryList.addAll(generalIconsList);


        gridIconsList.addAll(mainCategoryList);


        mainCategoryList.clear();
        mainCategoryList.addAll(gridIconsList);
//        mainCategoryList.addAll(newBannerList);
//        }

        return newBannerList;
    }

    @Override
    public void onItemClick(int position) {
        onItemClick(position, "HOME");
    }

    public void onItemClick(int position, String type) {

        Utils.hideKeyboard(getActContext());

        HashMap<String, String> mapData = null;
        if (type.equalsIgnoreCase("HOME")) {
            mapData = generalCategoryList.get(position);
        } else {
            mapData = allMainCategoryList.get(position);
        }


        if (mapData.get("eCatType") != null) {
            Bundle bn = new Bundle();
            switch (mapData.get("eCatType").toUpperCase()) {
                case "MORE":
                    openMoreDialog();
                    break;
                case "RIDE":
                    bn.putString("selType", Utils.CabGeneralType_Ride);
                    bn.putBoolean("isRestart", false);
                    break;
                case "MOTORIDE":
                    bn.putString("selType", Utils.CabGeneralType_Ride);
                    bn.putBoolean("isRestart", false);
                    bn.putBoolean("emoto", true);
                    break;
                case "DELIVERY":
                    bn.putString("selType", Utils.CabGeneralType_Deliver);
                    bn.putBoolean("isRestart", false);
                    break;
                case "MOTODELIVERY":
                    bn.putString("selType", Utils.CabGeneralType_Deliver);
                    bn.putBoolean("isRestart", false);
                    bn.putBoolean("emoto", true);
                    break;
                case "RENTAL":
                    bn.putString("selType", "rental");
                    bn.putBoolean("isRestart", false);
                    break;
                case "MOTORENTAL":
                    bn.putString("selType", "rental");
                    bn.putBoolean("isRestart", false);
                    bn.putBoolean("emoto", true);
                    break;
            }

            if (!mapData.get("eCatType").equalsIgnoreCase("More")) {
                new StartActProcess(getActContext()).startActWithData(MainActivity.class, bn);
            }

            return;
        }


        if (CAT_TYPE_MODE.equals("0")) {

            setSubCategoryList(mapData);

            return;
        }/* else if(generalFunc.getJsonValue(CommonUtilities.UBERX_PARENT_CAT_ID, userProfileJson).equalsIgnoreCase("0")){
            setMainCategory();
        }*/


       if (latitude.equalsIgnoreCase("0.0") || longitude.equalsIgnoreCase("0.0")) {
            generalFunc.showMessage(generalFunc.getCurrentView((Activity) getActContext()), generalFunc.retrieveLangLBl("", "LBL_SET_LOCATION"));
        } else {
            checkServiceAvailableOrNot(mapData.get("iVehicleCategoryId"), latitude, longitude, position);
        }
    }

    public void setMainCategory() {
        CAT_TYPE_MODE = "0";


        ((CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar)).setLayoutTransition(new LayoutTransition());
        generalCategoryList.clear();

        generalCategoryList.addAll(mainCategoryList);
        ufxCatAdapter = null;
        ufxCatAdapter = new UberXCategoryAdapter(getActContext(), generalCategoryList, generalFunc);

        ufxCatAdapter.setCategoryMode("0");

        ufxCatAdapter.setOnItemClickList(UberXActivity.this);
        dataListRecyclerView.setAdapter(ufxCatAdapter);

        if (generalFunc.getJsonValue("APP_TYPE", userProfileJson).equalsIgnoreCase(Utils.CabGeneralTypeRide_Delivery_UberX)) {
            bannerArea.setVisibility(View.GONE);
            rduTopArea.setVisibility(View.VISIBLE);
            headerLogo.setVisibility(View.VISIBLE);
            uberXHeaderLayout.setVisibility(View.GONE);
            selectServiceTxt.setVisibility(View.GONE);
//            bannerListRecyclerView.setVisibility(View.VISIBLE);
//            rduNestedScrollView.fullScroll(View.FOCUS_UP);


        } else {
            bannerArea.setVisibility(View.VISIBLE);
            rduTopArea.setVisibility(View.GONE);
            headerLogo.setVisibility(View.GONE);
            uberXHeaderLayout.setVisibility(View.VISIBLE);
            selectServiceTxt.setVisibility(View.VISIBLE);


        }
        selectServiceTxt.setText(generalFunc.retrieveLangLBl("Select Service", "LBL_SELECT_SERVICE_TXT"));

        setParentCategoryLayoutManager();

        dataListRecyclerView.removeItemDecoration(itemDecoration);

        ufxCatAdapter.notifyDataSetChanged();
        if (addDrawer != null) {
            addDrawer.setMenuState(true);
        }

    }


    public void setSubCategoryList(HashMap<String, String> dataItem) {

        Utils.printLog("setSubCategoryList", "::Yes::");
        ufxCatAdapter.setCategoryMode("1");


        rduTopArea.setVisibility(View.GONE);


        headerLogo.setVisibility(View.GONE);
        uberXHeaderLayout.setVisibility(View.VISIBLE);
        selectServiceTxt.setVisibility(View.VISIBLE);


        CAT_TYPE_MODE = "1";
        dataListRecyclerView.setLayoutManager(new LinearLayoutManager(getActContext()));
        if (generalFunc.getJsonValue(CommonUtilities.UBERX_PARENT_CAT_ID, userProfileJson).equalsIgnoreCase("0")) {
            ((CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar)).setLayoutTransition(null);
            bannerArea.setVisibility(View.GONE);
            selectServiceTxt.setText(dataItem.get("vCategory"));
        }
        String iVehicleCategoryId = dataItem.get("iVehicleCategoryId");

        getCategory(iVehicleCategoryId, "1");

        if (addDrawer != null) {
            addDrawer.setMenuState(false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {

            if (addDrawer != null) {
                this.userProfileJson = generalFunc.retrieveValue(CommonUtilities.USER_PROFILE_JSON);
                obj_userProfile = generalFunc.getJsonObject(userProfileJson);
                addDrawer.userProfileJson = this.userProfileJson;
                addDrawer.obj_userProfile = generalFunc.getJsonObject(this.userProfileJson);
                addDrawer.buildDrawer();
            }

            ConfigPubNub.getInstance().unRegisterGcmReceiver();
            ConfigPubNub.getInstance().ConfigPubNub(getActContext());
            ConfigPubNub.getInstance().setTripId("", "");

            if (updateBannerChangeFreqTask != null) {
                updateBannerChangeFreqTask.startRepeatingTask();
            }

            if (generalFunc.retrieveValue(CommonUtilities.ISWALLETBALNCECHANGE).equalsIgnoreCase("Yes")) {
                getWalletBalDetails();
            } else {
                userProfileJson = generalFunc.retrieveValue(CommonUtilities.USER_PROFILE_JSON);
                obj_userProfile = generalFunc.getJsonObject(userProfileJson);
                setUserInfo();
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onTaskRun() {

        if (currentBannerPosition < (bannerAdapter.getCount() - 1)) {
            bannerViewPager.setCurrentItem((bannerViewPager.getCurrentItem() + 1), true);
        } else {
            bannerViewPager.setCurrentItem(0, true);

        }

    }

    @Override
    protected void onPause() {
        super.onPause();

        if (updateBannerChangeFreqTask != null) {
            updateBannerChangeFreqTask.stopRepeatingTask();
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        currentBannerPosition = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public void getWalletBalDetails() {


        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "GetMemberWalletBalance");
        parameters.put("iUserId", generalFunc.getMemberId());
        parameters.put("UserType", CommonUtilities.app_type);

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(getActContext(), parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(responseString -> {


            if (responseString != null && !responseString.equals("")) {

                boolean isDataAvail = GeneralFunctions.checkDataAvail(CommonUtilities.action_str, responseString);

                if (isDataAvail) {
                    try {
                        generalFunc.storedata(CommonUtilities.ISWALLETBALNCECHANGE, "No");
                        String userProfileJson = generalFunc.retrieveValue(CommonUtilities.USER_PROFILE_JSON);
                        JSONObject object = generalFunc.getJsonObject(userProfileJson);
                        object.put("user_available_balance", generalFunc.getJsonValue("MemberBalance", responseString));
                        generalFunc.storedata(CommonUtilities.USER_PROFILE_JSON, object.toString());

                        userProfileJson = generalFunc.retrieveValue(CommonUtilities.USER_PROFILE_JSON);
                        obj_userProfile = generalFunc.getJsonObject(userProfileJson);


                        setUserInfo();
                    } catch (Exception e) {

                    }

                }


            }
        });
        exeWebServer.execute();
    }


    public void setUserInfo() {
        View view = ((Activity) getActContext()).findViewById(android.R.id.content);
        ((MTextView) view.findViewById(R.id.userNameTxt)).setText(generalFunc.getJsonValueStr("vName", obj_userProfile) + " "
                + generalFunc.getJsonValueStr("vLastName", obj_userProfile));
        ((MTextView) view.findViewById(R.id.walletbalncetxt)).setText(generalFunc.retrieveLangLBl("", "LBL_WALLET_BALANCE") + ": " + generalFunc.convertNumberWithRTL(generalFunc.getJsonValueStr("user_available_balance", obj_userProfile)));

        generalFunc.checkProfileImage((SelectableRoundedImageView) view.findViewById(R.id.userImgView), userProfileJson, "vImgName");
    }


    @Override
    public void onAddressFound(String address, double latitude, double longitude, String geocodeobject) {
        if (isback) {
            if (getIntent().getStringExtra("address") != null) {
                return;
            }
        }

        if (address != null && !address.equals("")) {
            this.latitude = latitude + "";
            this.longitude = longitude + "";
            headerLocAddressTxt.setText(address);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Utils.MY_PROFILE_REQ_CODE && resultCode == RESULT_OK && data != null) {
            userProfileJson = generalFunc.retrieveValue(CommonUtilities.USER_PROFILE_JSON);

            addDrawer.changeUserProfileJson(userProfileJson);
        } else if (requestCode == Utils.UBER_X_SEARCH_PICKUP_LOC_REQ_CODE && resultCode == RESULT_OK && data != null) {

            headerLocAddressTxt.setText(data.getStringExtra("Address"));
            this.latitude = data.getStringExtra("Latitude") == null ? "0.0" : data.getStringExtra("Latitude");
            this.longitude = data.getStringExtra("Longitude") == null ? "0.0" : data.getStringExtra("Longitude");

            if (!this.latitude.equalsIgnoreCase("0.0") && !this.longitude.equalsIgnoreCase("0.0")) {
                isUfxaddress = true;
            }


        } else if (requestCode == Utils.CARD_PAYMENT_REQ_CODE && resultCode == RESULT_OK && data != null) {
            userProfileJson = generalFunc.retrieveValue(CommonUtilities.USER_PROFILE_JSON);

            addDrawer.changeUserProfileJson(userProfileJson);
        } else if (requestCode == Utils.REQUEST_CODE_GPS_ON) {

            isgpsview = true;

            if (generalFunc.isLocationEnabled()) {

                if (getLastLocation != null) {
                    getLastLocation.stopLocationUpdates();
                    getLastLocation = null;
                }
                getLastLocation = new GetLocationUpdates(getActContext(), Utils.LOCATION_UPDATE_MIN_DISTANCE_IN_MITERS, false, this);


                if (getLastLocation != null) {


                    final Handler handler = new Handler();
                    int delay = 1000; //milliseconds

                    handler.postDelayed(new Runnable() {
                        public void run() {
                            isgpsview = true;
                            //do something
                            if (getLastLocation.getLastLocation() != null) {
                                isgpsview = false;
                                NoLocationView();
                            } else {
                                handler.postDelayed(this, 1000);
                            }
                        }
                    }, delay);
                }


            } else {
                isgpsview = false;
            }

        } else if (requestCode == Utils.REQUEST_CODE_NETWOEK_ON) {

            setNoLocViewEnableOrDisabled(true);


            final Handler handler = new Handler();
            int delay = 1000; //milliseconds

            handler.postDelayed(new Runnable() {
                public void run() {


                    if (intCheck.isNetworkConnected() && intCheck.check_int()) {

                        setNoLocViewEnableOrDisabled(false);

                    } else {

                        setNoLocViewEnableOrDisabled(true);
                        handler.postDelayed(this, 1000);

                    }
                    //

                }
            }, delay);


        } else if (requestCode == Utils.SEARCH_PICKUP_LOC_REQ_CODE && resultCode == RESULT_OK && data != null) {

//            mainAct.configPickUpDrag(true, false, false);

            if (resultCode == RESULT_OK) {

                isUfxaddress = true;

                noloactionview.setVisibility(View.GONE);

                Place place = PlaceAutocomplete.getPlace(getActContext(), data);

                LatLng placeLocation = place.getLatLng();


                headerLocAddressTxt.setText(place.getAddress().toString());
                this.latitude = placeLocation.latitude + "";
                this.longitude = placeLocation.longitude + "";


            }

        }
    }

    public void pubNubMsgArrived(final String message) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                String msgType = generalFunc.getJsonValue("MsgType", message);
                String iDriverId = generalFunc.getJsonValue("iDriverId", message);

                if (msgType.equals("DriverArrived")) {
                    if (!isArrivedPopup) {
                        isArrivedPopup = true;
                    }
                } else {
                    onGcmMessageArrived(message);
                }


            }
        });
    }


    public void onGcmMessageArrived(String message) {

        String driverMsg = generalFunc.getJsonValue("Message", message);

//        if (!tripDetail.get("iTripId").equals(generalFunc.getJsonValue("iTripId", message))) {
//            return;
//        }

        if (driverMsg.equals("TripEnd")) {

// show alert - - not cancabable - ok button only - on click OK finish screen


            if (!isEndpopup) {
                isEndpopup = true;
                // Utils.generateNotification(getActContext(), generalFunc.retrieveLangLBl("", "LBL_END_TRIP_DIALOG_TXT"));
                //   buildMessage(generalFunc.retrieveLangLBl("", "LBL_END_TRIP_DIALOG_TXT"), generalFunc.retrieveLangLBl("", "LBL_BTN_OK_TXT"));
            }

        } else if (driverMsg.equals("TripStarted")) {
            if (!isstartPopup) {
                isstartPopup = true;
                // Utils.generateNotification(getActContext(), generalFunc.retrieveLangLBl("", "LBL_START_TRIP_DIALOG_TXT"));
                // buildMessage(generalFunc.retrieveLangLBl("", "LBL_START_TRIP_DIALOG_TXT"), generalFunc.retrieveLangLBl("", "LBL_BTN_OK_TXT"));
            }


        } else if (driverMsg.equals("TripCancelledByDriver")) {

            if (!isCancelpopup) {
                isCancelpopup = true;
                String reason = generalFunc.getJsonValue("Reason", message);
                //  Utils.generateNotification(getActContext(), generalFunc.retrieveLangLBl("", "LBL_PREFIX_TRIP_CANCEL_DRIVER") + " " + reason);

                // buildMessage((generalFunc.retrieveLangLBl("", generalFunc.retrieveLangLBl("", "LBL_PREFIX_TRIP_CANCEL_DRIVER") )+ " " + reason), generalFunc.retrieveLangLBl("", "LBL_BTN_OK_TXT"));
            }


        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (configPubNub != null) {
            configPubNub.releaseInstances();
        }

        if (getLastLocation != null) {
            getLastLocation.stopLocationUpdates();
        }
        releaseResources();

    }

    public void configCategoryView() {
        setMainCategory();
    }

    @Override
    public void onBackPressed() {

        if (CAT_TYPE_MODE.equals("1") && generalFunc.getJsonValue(CommonUtilities.UBERX_PARENT_CAT_ID, userProfileJson).equalsIgnoreCase("0")) {
            configCategoryView();
            return;
        }

        super.onBackPressed();

    }


    public void releaseResources() {
        if (getAddressFromLocation != null) {
            getAddressFromLocation.setAddressList(null);
            getAddressFromLocation = null;
        }

    }

    @Override
    public void onLocationUpdate(Location mLastLocation) {

        latitude = mLastLocation.getLatitude() + "";
        longitude = mLastLocation.getLongitude() + "";

        getAddressFromLocation = new GetAddressFromLocation(getActContext(), generalFunc);
        getAddressFromLocation.setLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        getAddressFromLocation.setAddressList(this);
        getAddressFromLocation.execute();

    }


    public void checkServiceAvailableOrNot(String iVehicleCategoryId, String latitude, String longitude, int position)

    {

        HashMap<String, String> parameters = new HashMap<String, String>();

        parameters.put("type", "getServiceCategoryTypes");
        parameters.put("iVehicleCategoryId", iVehicleCategoryId);
        parameters.put("userId", generalFunc.getMemberId());
        parameters.put("vLatitude", latitude);
        parameters.put("vLongitude", longitude);
        parameters.put("UserType", Utils.userType);
        parameters.put("eCheck", "Yes");

        final ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(getActContext(), parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setCancelAble(false);
        exeWebServer.setDataResponseListener(responseString -> {


            if (responseString != null && !responseString.equals("")) {

                boolean isDataAvail = generalFunc.checkDataAvail(CommonUtilities.action_str, responseString);

                if (isDataAvail) {

                    Bundle bundle = new Bundle();
                    // bundle.putString("USER_PROFILE_JSON", userProfileJson);
                    bundle.putString("iVehicleCategoryId", generalCategoryList.get(position).get("iVehicleCategoryId"));
                    bundle.putString("vCategoryName", generalCategoryList.get(position).get("vCategory"));
                    bundle.putString("latitude", latitude);
                    bundle.putString("longitude", longitude);
                    bundle.putString("address", headerLocAddressTxt.getText().toString());
                    new StartActProcess(getActContext()).startActWithData(UberXSelectServiceActivity.class, bundle);


                } else {

                    generalFunc.showGeneralMessage("", generalFunc.retrieveLangLBl("", generalFunc.getJsonValue("message", responseString)));
                }
            } else {
                generalFunc.showError();
            }
        });
        exeWebServer.execute();
    }


    public class setOnClickList implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            int i = view.getId();
            Bundle bn = new Bundle();

            if (i == settingTxt.getId()) {
//                isgpsview = true;
//                new StartActProcess(getActContext()).
//                        startActForResult(Settings.ACTION_LOCATION_SOURCE_SETTINGS, Utils.REQUEST_CODE_GPS_ON);


                if (!intCheck.isNetworkConnected()) {

                    new StartActProcess(getActContext()).
                            startActForResult(Settings.ACTION_SETTINGS, Utils.REQUEST_CODE_NETWOEK_ON);

                } else {
                    isgpsview = true;

                    new StartActProcess(getActContext()).
                            startActForResult(Settings.ACTION_LOCATION_SOURCE_SETTINGS, Utils.REQUEST_CODE_GPS_ON);

                }

            } else if (i == pickupredirectTxt.getId()) {
                try {
                    LatLngBounds bounds = null;
                    Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .setBoundsBias(bounds)
                            .build(UberXActivity.this);
                    startActivityForResult(intent, Utils.SEARCH_PICKUP_LOC_REQ_CODE);
                } catch (Exception e) {

                }

            } else if (i == nolocbackImgView.getId()) {
                nolocmenuImgView.setVisibility(View.VISIBLE);
                nolocbackImgView.setVisibility(View.GONE);


            } else if (i == nolocmenuImgView.getId()) {
                addDrawer.checkDrawerState(true);
            } else if (i == walletArea.getId()) {
                setBounceAnimation(walletArea, () -> {
                    new StartActProcess(getActContext()).startActWithData(MyWalletActivity.class, bn);
                });

            } else if (i == ongoingJobArea.getId()) {
                setBounceAnimation(ongoingJobArea, () -> {
                    bn.putBoolean("isRestart", false);
                    new StartActProcess(getActContext()).startActForResult(OnGoingTripsActivity.class, bn, Utils.ASSIGN_DRIVER_CODE);
                });
            } else if (i == profileArea.getId()) {
                setBounceAnimation(profileArea, () -> {
                    addDrawer.openMenuProfile();
                });
            } else if (i == historyArea.getId()) {
                setBounceAnimation(historyArea, () -> {
                    new StartActProcess(getActContext()).startActWithData(HistoryActivity.class, bn);
                });
            }

        }
    }

    private void setBounceAnimation(View view, BounceAnimListener bounceAnimListener) {
        Animation anim = AnimationUtils.loadAnimation(getActContext(), R.anim.bounce_interpolator);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

                if (bounceAnimListener != null) {
                    bounceAnimListener.onAnimationFinished();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(anim);
    }

    private interface BounceAnimListener {
        void onAnimationFinished();
    }

    public class setOnClickLst implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Utils.hideKeyboard(getActContext());
            switch (v.getId()) {
                case R.id.uberXHeaderLayout:

                    if (generalFunc.isLocationEnabled()) {
                        Bundle bn = new Bundle();

//                if (mainAct.getPickUpLocation() !=null) {
//                    bn.putString("PickUpLatitude", "" + mainAct.getPickUpLocation().getLatitude());
//                    bn.putString("PickUpLongitude", "" + mainAct.getPickUpLocation().getLongitude());
//                }
                        bn.putString("locationArea", "source");
                        bn.putBoolean("isaddressview", true);
                        bn.putDouble("lat", GeneralFunctions.parseDoubleValue(0.0, latitude));
                        bn.putDouble("long", GeneralFunctions.parseDoubleValue(0.0, longitude));
                        bn.putString("address", headerLocAddressTxt.getText().toString() + "");

                        new StartActProcess(getActContext()).startActForResult(SearchLocationActivity.class,
                                bn, Utils.UBER_X_SEARCH_PICKUP_LOC_REQ_CODE);
                    } else {
                        try {
                            LatLngBounds bounds = null;


                            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                    .setBoundsBias(bounds)
                                    .build(UberXActivity.this);
                            startActivityForResult(intent, Utils.SEARCH_PICKUP_LOC_REQ_CODE);
                        } catch (Exception e) {

                        }
                    }
                    break;

                case R.id.backImgView:
                    onBackPressed();
                    break;
            }
        }
    }


    public void openMoreDialog() {

        BottomSheetDialog moreDialog = new BottomSheetDialog(getActContext());


        View contentView = View.inflate(getActContext(), R.layout.dialog_more, null);


        moreDialog.setContentView(contentView);
        moreDialog.setCancelable(false);
//        faredialog.setContentView(contentView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
//                700));
        BottomSheetBehavior mBehavior = BottomSheetBehavior.from((View) contentView.getParent());
        mBehavior.setPeekHeight(Utils.dipToPixels(getActContext(), 320));
        mBehavior.setHideable(true);
        MTextView moreTitleTxt = contentView.findViewById(R.id.moreTitleTxt);
        MTextView cancelTxt = contentView.findViewById(R.id.cancelTxt);

        moreTitleTxt.setText(generalFunc.retrieveLangLBl("", "LBL_SELECT_SERVICE"));
        cancelTxt.setText(generalFunc.retrieveLangLBl("", "LBL_CANCEL_TXT"));
        cancelTxt.setOnClickListener(v -> moreDialog.dismiss());

        RecyclerView dataListRecyclerView_more = contentView.findViewById(R.id.dataListRecyclerView_more);
        dataListRecyclerView_more.setLayoutManager(new GridLayoutManager(getActContext(), getNumOfColumns()));
        UberXCategoryAdapter ufxCatAdapter = new UberXCategoryAdapter(getActContext(), allMainCategoryList, generalFunc, true);
        dataListRecyclerView_more.setAdapter(ufxCatAdapter);

        ufxCatAdapter.setOnItemClickList(position -> {
                    moreDialog.dismiss();
                    Utils.printLog("MOreSelectedItem", "Position:" + position + "::ItemDesc::" + allMainCategoryList.get(position).toString());
                    onItemClick(position, "MORE");
                }
        );


        moreDialog.show();


    }


    public void setNoLocViewEnableOrDisabled(boolean show) {

        if (show) {
            noloactionview.setVisibility(View.VISIBLE);
            if (intCheck.isNetworkConnected() != true) {
                setLocationTitles(false);
            } else {
                setLocationTitles(true);

            }

        } else {

            if (noInternetConn != null) {
                noInternetConn.setTaskKilledValue(true);
                noInternetConn = null;
            }
            noloactionview.setVisibility(View.GONE);

        }


    }

    private void setLocationTitles(boolean setLocDetails) {
        if (setLocDetails) {
            noLocImgView.setImageDrawable(getActContext().getResources().getDrawable(R.mipmap.ic_gps_off));
            noLocTitleTxt.setText(generalFunc.retrieveLangLBl("", "LBL_LOCATION_SERVICES_TURNED_OFF"));
            noLocMsgTxt.setText(generalFunc.retrieveLangLBl("", "LBL_LOCATION_SERVICES_TURNED_OFF_DETAILS"));
            settingTxt.setText(generalFunc.retrieveLangLBl("", "LBL_TURN_ON_LOC_SERVICE"));
            pickupredirectTxt.setText(generalFunc.retrieveLangLBl("Enter pickup address", "LBL_ENTER_PICK_UP_ADDRESS"));
            pickupredirectTxt.setVisibility(View.VISIBLE);
            pickupredirectTxt.setOnClickListener(new setOnClickList());
        } else {
            noLocTitleTxt.setText(generalFunc.retrieveLangLBl("", "LBL_NO_INTERNET_TITLE"));
            noLocMsgTxt.setText(generalFunc.retrieveLangLBl("", "LBL_NO_INTERNET_SUB_TITLE"));
            settingTxt.setText(generalFunc.retrieveLangLBl("", "LBL_SETTINGS"));
            pickupredirectTxt.setVisibility(View.GONE);
            noLocImgView.setImageDrawable(getActContext().getResources().getDrawable(R.mipmap.ic_wifi_off));
            pickupredirectTxt.setOnClickListener(null);

        }
    }

    public void setNoGpsViewEnableOrDisabled(boolean show) {

        if (true) {
            setLocationTitles(true);
        }
        NoLocationView();
    }


    public Integer getNumOfColumns() {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        float dpWidth = (displayMetrics.widthPixels - Utils.dipToPixels(getActContext(), 10)) / displayMetrics.density;
        int noOfColumns = (int) (dpWidth / 74);
        return noOfColumns;
    }

}
