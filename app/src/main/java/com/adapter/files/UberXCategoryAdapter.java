package com.adapter.files;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;

import com.mobisquid.user.R;
import com.general.files.GeneralFunctions;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.utils.CommonUtilities;
import com.utils.Utils;
import com.view.CreateRoundedView;
import com.view.MTextView;
import com.view.SelectableRoundedImageView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Admin on 02-03-2017.
 */
public class UberXCategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int TYPE_GRID = 0;
    private final int TYPE_BANNER = 1;
    private final int NO_TYPE = -1;

    public GeneralFunctions generalFunc;
    ArrayList<HashMap<String, String>> list_item;
    Context mContext;
    OnItemClickList onItemClickList;

    String CAT_TYPE_MODE = "0";

    boolean isForceToGrid = false;
    int positionOfSeperatorView = -1;

    public UberXCategoryAdapter(Context mContext, ArrayList<HashMap<String, String>> list_item, GeneralFunctions generalFunc) {
        this.mContext = mContext;
        this.list_item = list_item;
        this.generalFunc = generalFunc;
    }

    public UberXCategoryAdapter(Context mContext, ArrayList<HashMap<String, String>> list_item, GeneralFunctions generalFunc, boolean isForceToGrid) {
        this.mContext = mContext;
        this.list_item = list_item;
        this.generalFunc = generalFunc;
        this.isForceToGrid = isForceToGrid;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        if (viewType == TYPE_BANNER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rdu_banner_design, parent, false);
            BannerViewHolder bannerViewHolder = new BannerViewHolder(view);
            return bannerViewHolder;
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(CAT_TYPE_MODE.equals("0") ? R.layout.item_uberx_cat_grid_design : R.layout.item_uberx_cat_list_design, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }


    }

    public void setCategoryMode(String CAT_TYPE_MODE) {
        this.CAT_TYPE_MODE = CAT_TYPE_MODE;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder parentViewHolder, final int position) {

        HashMap<String, String> item = list_item.get(position);
        if (parentViewHolder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) parentViewHolder;
            if (item.get("vCategory").matches("\\w*")) {
                viewHolder.uberXCatNameTxtView.setMaxLines(1);

                viewHolder.uberXCatNameTxtView.setText(item.get("vCategory"));
            } else {
                viewHolder.uberXCatNameTxtView.setMaxLines(2);

                viewHolder.uberXCatNameTxtView.setText(item.get("vCategory"));
            }

            Picasso.with(mContext).load(item.get("vLogo_image").equals("") ? CommonUtilities.SERVER : item.get("vLogo_image")).placeholder(R.mipmap.ic_no_icon).into(viewHolder.catImgView, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {

                    if (!item.get("vLogo_image").contains("http") && !item.get("vLogo_image").equals("")) {
                        Picasso.with(mContext).load(GeneralFunctions.parseIntegerValue(0, item.get("vLogo_image"))).placeholder(R.mipmap.ic_no_icon).error(R.mipmap.ic_no_icon).into(viewHolder.catImgView);
                    }
                }
            });


            viewHolder.contentArea.setOnClickListener(view -> {
                if (onItemClickList != null) {
                    onItemClickList.onItemClick(position);
                }
            });

            if (CAT_TYPE_MODE.equals("0")) {
                viewHolder.contentArea.setOnTouchListener((view, motionEvent) -> {
                    switch (motionEvent.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            scaleView(view, 0.85f, 0.85f);
                            break;
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL:
                            scaleView(view, (float) 1.0, (float) 1.0);
                            break;
                    }
                    return false;
                });
            } else {
                viewHolder.contentArea.setOnTouchListener(null);
            }

        } else {
            BannerViewHolder holder = (BannerViewHolder) parentViewHolder;


            holder.seperatorView.setVisibility(View.GONE);

            if (positionOfSeperatorView == -1 || positionOfSeperatorView == position) {
                holder.seperatorView.setVisibility(View.VISIBLE);
                positionOfSeperatorView = position;
            }

            holder.serviceNameTxt.setText(list_item.get(position).get("vCategoryBanner").equalsIgnoreCase("") ? list_item.get(position).get("vCategory") : list_item.get(position).get("vCategoryBanner"));

            holder.bookNowTxt.setText(list_item.get(position).get("LBL_BOOK_NOW"));

            CardView.LayoutParams layoutParams = (CardView.LayoutParams) holder.bannerAreaContainerView.getLayoutParams();

//            layoutParams.height = (int) (((Utils.getScreenPixelWidth(mContext) - Utils.dipToPixels(mContext, 20)) / 1.77777778));
            layoutParams.height = Utils.getHeightOfBanner(mContext, mContext.getResources().getDimensionPixelSize(R.dimen.category_banner_left_right_margin) * 2);

            new CreateRoundedView(mContext.getResources().getColor(R.color.appThemeColor_1), Utils.dipToPixels(mContext, 8), Utils.dipToPixels(mContext, 0), mContext.getResources().getColor(R.color.appThemeColor_1), holder.serviceNameTxt);


            new CreateRoundedView(mContext.getResources().getColor(R.color.appThemeColor_2), Utils.dipToPixels(mContext, 8), Utils.dipToPixels(mContext, 0), mContext.getResources().getColor(R.color.appThemeColor_1), holder.bookNowTxt);

            Picasso.with(mContext).load(item.get("vBannerImage").equals("") ? CommonUtilities.SERVER : item.get("vBannerImage")).placeholder(R.mipmap.ic_no_icon).into(holder.bannerImgView, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {

                    if (!item.get("vBannerImage").contains("http") && !item.get("vBannerImage").equals("")) {
                        Picasso.with(mContext).load(GeneralFunctions.parseIntegerValue(0, item.get("vBannerImage"))).placeholder(R.mipmap.ic_no_icon).error(R.mipmap.ic_no_icon).into(holder.bannerImgView);
                    }
                }
            });

            holder.containerView.setOnClickListener(view -> {
                if (onItemClickList != null) {
                    onItemClickList.onItemClick(position);
                }
            });

            if (CAT_TYPE_MODE.equals("0")) {

                holder.containerView.setOnTouchListener((view, motionEvent) -> {
                    switch (motionEvent.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            scaleView(view, 0.97f, 0.97f);
                            break;
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL:
                            scaleView(view, (float) 1.0, (float) 1.0);
                            break;
                    }
                    return false;
                });
            } else {
                Utils.printLog("TouchOnBanner","Removed");
                holder.containerView.setOnTouchListener(null);
            }


//            holder.bannerImgView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (onItemClickList != null) {
//                        onItemClickList.onBannerItemClick(position);
//                    }
//                }
//            });
        }


    }

    public void scaleView(View v, float startScale, float endScale) {
        Animation anim = new ScaleAnimation(
                startScale, endScale, // Start and end values for the X axis scaling
                startScale, endScale, // Start and end values for the Y axis scaling
                Animation.RELATIVE_TO_SELF, 0.5f, // Pivot point of X scaling
                Animation.RELATIVE_TO_SELF, 0.5f); // Pivot point of Y scaling
        anim.setFillAfter(true); // Needed to keep the result of the animation
        anim.setDuration(100);
        v.startAnimation(anim);
    }

    @Override
    public int getItemCount() {
        return list_item.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (CAT_TYPE_MODE.equals("0")) {

            HashMap<String, String> dataMap = list_item.get(position);
            if (dataMap.get("eShowType") != null && !dataMap.get("eShowType").equals("") && !dataMap.get("eShowType").equalsIgnoreCase("ICON") && isForceToGrid == false) {
                return TYPE_BANNER;
            } else {
                return TYPE_GRID;
            }
        }
        return NO_TYPE;
    }

    public void setOnItemClickList(OnItemClickList onItemClickList) {
        this.onItemClickList = onItemClickList;
    }

    public interface OnItemClickList {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public MTextView uberXCatNameTxtView;

        public View contentArea;
        public ImageView catImgView;

        public ViewHolder(View view) {
            super(view);

            uberXCatNameTxtView = (MTextView) view.findViewById(R.id.uberXCatNameTxtView);
            contentArea = view.findViewById(R.id.contentArea);
            catImgView = (ImageView) view.findViewById(R.id.catImgView);
        }
    }

    public class BannerViewHolder extends RecyclerView.ViewHolder {


        public View containerView;
        public View bannerAreaContainerView;
        public View seperatorView;
        public SelectableRoundedImageView bannerImgView;
        public MTextView bookNowTxt, serviceNameTxt;

        public BannerViewHolder(View view) {
            super(view);
            containerView =  view.findViewById(R.id.containerView);
            bannerImgView = (SelectableRoundedImageView) view.findViewById(R.id.bannerImgView);
            bookNowTxt = (MTextView) view.findViewById(R.id.bookNowTxt);
            serviceNameTxt = (MTextView) view.findViewById(R.id.serviceNameTxt);
            bannerAreaContainerView = view.findViewById(R.id.bannerAreaContainerView);
            seperatorView = view.findViewById(R.id.seperatorView);
        }
    }


}
