package com.mobisquid.user;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.adapter.files.OnGoingTripDetailAdapter;
import com.dialogs.OpenTutorDetailDialog;
import com.fragments.CustomSupportMapFragment;
import com.general.files.ConfigPubNub;
import com.general.files.ExecuteWebServerUrl;
import com.general.files.GeneralFunctions;
import com.general.files.StartActProcess;
import com.general.files.UpdateDirections;
import com.general.files.UpdateFrequentTask;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.utils.AnimateMarker;
import com.utils.CommonUtilities;
import com.utils.Utils;
import com.view.ErrorView;
import com.view.GenerateAlertBox;
import com.view.MTextView;
import com.view.SelectableRoundedImageView;
import com.view.simpleratingbar.SimpleRatingBar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.utils.CommonUtilities.APP_TYPE;

/**
 * Created by Admin on 22-02-2017.
 */
public class OnGoingTripDetailsActivity extends AppCompatActivity implements OnMapReadyCallback, UpdateFrequentTask.OnTaskRunCalled {


    ProgressBar loading_ongoing_trips_detail;
    ErrorView errorView;
    RecyclerView onGoingTripsDetailListRecyclerView;
    ImageView backImgView;
    SelectableRoundedImageView user_img;
    MTextView userNameTxt, userAddressTxt, subTitleTxt, titleTxt;
    SimpleRatingBar ratingBar;
    OnGoingTripDetailAdapter onGoingTripDetailAdapter;
    ArrayList<HashMap<String, String>> list;
    HashMap<String, String> tripDetail = new HashMap<>();
    HashMap<String, String> tempMap;
    GeneralFunctions generalFunc;
    String server_time = "";
    String userProfileJson = "";
    MTextView progressHinttext;
    String driverStatus = "";

    LatLng driverLocation;
    Marker driverMarker;
    int DRIVER_LOC_FETCH_TIME_INTERVAL;

    CustomSupportMapFragment map;
    boolean isTaskKilled = false;
    GoogleMap gMap;
    LinearLayout googlemaparea;
    boolean isarrived = false;
    boolean isarrivedpopup = false;
    boolean isstartpopup = false;

    MTextView timeTxt;
    boolean isDriverArrived = false;
    String eType = "";
    UpdateDirections updateDirections;
    Location destLoc;
    View marker_view = null;
    SelectableRoundedImageView providerImgView = null;
    AnimateMarker animDriverMarker;
    boolean ishowdialog = false;
    private LinearLayout contentArea;

    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_ongoing_trip_details);

        generalFunc = new GeneralFunctions(getActContext());
        animDriverMarker = new AnimateMarker();
        map = (CustomSupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapV2);
        map.getMapAsync(this);
        init();

        setData();


        progressHinttext.setText(generalFunc.retrieveLangLBl("JOB PROGRESS", "LBL_PROGRESS_HINT"));
        setLables();

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);


    }

    public void callUpdateDeirection(Location driverlocation) {
        if (destLoc == null) {
            return;

        }
        if (updateDirections == null) {

            updateDirections = new UpdateDirections(getActContext(), null, driverlocation, destLoc);
            updateDirections.scheduleDirectionUpdate();
        } else {
            updateDirections.changeUserLocation(driverlocation);
        }

    }

    public boolean isPubNubEnabled() {
        String ENABLE_PUBNUB = generalFunc.getJsonValue("ENABLE_PUBNUB", userProfileJson);

        return ENABLE_PUBNUB.equalsIgnoreCase("Yes");

    }

    public void setData() {
        tripDetail = (HashMap<String, String>) getIntent().getSerializableExtra("TripDetail");

        userProfileJson = generalFunc.retrieveValue(CommonUtilities.USER_PROFILE_JSON);

        setDriverDetail();

        getTripDeliveryLocations();

        setLables();

        ConfigPubNub.getInstance().unRegisterGcmReceiver();
        ConfigPubNub.getInstance().ConfigPubNub(getActContext());
        ConfigPubNub.getInstance().setTripId(tripDetail.get("iTripId"), tripDetail.get("iDriverId"));

        if (!tripDetail.get("driverStatus").equals("Arrived")) {
            if (isPubNubEnabled()) {
                subscribeToDriverLocChannel();
            }

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    LatLng driverLocation_update = new LatLng(generalFunc.parseDoubleValue(0.0, tripDetail.get("driverLatitude")),
                            generalFunc.parseDoubleValue(0.0, tripDetail.get("driverLongitude")));

                    updateDriverLocation(driverLocation_update);

                }
            }, 500);

        }
    }

    public void onResumeCalled() {


        subscribeToDriverLocChannel();
    }

    public void setTaskKilledValue(boolean isTaskKilled) {
        this.isTaskKilled = isTaskKilled;

        if (isTaskKilled == true) {
            onPauseCalled();
        }

    }

    public void onPauseCalled() {


        unSubscribeToDriverLocChannel();
    }

    public void subscribeToDriverLocChannel() {


        ArrayList<String> channelName = new ArrayList<>();
        channelName.add(Utils.pubNub_Update_Loc_Channel_Prefix + tripDetail.get("iDriverId"));
        ConfigPubNub.getInstance().subscribeToChannels(channelName);


    }

    public void unSubscribeToDriverLocChannel() {

        ArrayList<String> channelName = new ArrayList<>();
        channelName.add(Utils.pubNub_Update_Loc_Channel_Prefix + tripDetail.get("iDriverId"));
        ConfigPubNub.getInstance().unSubscribeToChannels(channelName);

    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MENU) {

            // perform your desired action here

            // return 'true' to prevent further propagation of the key event
            return true;
        }

        // let the system handle all other key events
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.my_ongoing_activity, menu);
        setLablesAsPerCurrentFrag(menu);

        return true;
    }

    @Override
    public void onOptionsMenuClosed(Menu menu) {

        setLablesAsPerCurrentFrag(menu);

        super.onOptionsMenuClosed(menu);
    }

    @Override
    public boolean onMenuOpened(int featureId, Menu menu) {
        setLablesAsPerCurrentFrag(menu);
        return super.onMenuOpened(featureId, menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        setLablesAsPerCurrentFrag(menu);

        return super.onPrepareOptionsMenu(menu);
    }

    public void setLablesAsPerCurrentFrag(Menu menu) {
        if (menu != null) {
            menu.findItem(R.id.menu_view_tutor_detail).setTitle(generalFunc.retrieveLangLBl("Driver/Service Provider", "LBL_MYTRIP_DRIVER"));

            if (driverStatus == null) {
                if (driverStatus.isEmpty() && getIntent().hasExtra("driverStatus")) {
                    driverStatus = getIntent().getStringExtra("driverStatus");
                }
            }

            if (Utils.checkText(driverStatus) && !driverStatus.equals("On Going Trip") && !driverStatus.equals("finished") && !driverStatus.equals("NONE") && !driverStatus.equals("Cancelled") && !driverStatus.equals("Canceled")) {
                menu.findItem(R.id.menu_cancel_trip).setVisible(true);
                menu.findItem(R.id.menu_cancel_trip).setTitle(generalFunc.retrieveLangLBl("Cancel Job", "LBL_CANCEL_BOOKING"));
            } else {
                menu.findItem(R.id.menu_cancel_trip).setVisible(false);

            }

            if (driverStatus.equals("Arrived")) {
                isarrived = true;

            }

            if (driverStatus.equals("On Going Trip")) {
                isarrived = true;

            }


            if (onGoingTripsDetailListRecyclerView.getVisibility() == View.VISIBLE) {
                menu.findItem(R.id.menu_track).setTitle(generalFunc.retrieveLangLBl("Live Track", "LBL_LIVE_TRACK_TXT"));
            } else {
                menu.findItem(R.id.menu_track).setTitle(generalFunc.retrieveLangLBl("JOB PROGRESS", "LBL_JOB_PROGRESS"));

            }

            menu.findItem(R.id.menu_call).setTitle(generalFunc.retrieveLangLBl("Call", "LBL_CALL_ACTIVE_TRIP"));
            menu.findItem(R.id.menu_message).setTitle(generalFunc.retrieveLangLBl("Message", "LBL_MESSAGE_ACTIVE_TRIP"));

            if (!tripDetail.get("eFareType").equals(Utils.CabFaretypeRegular)) {
                if (isarrived) {
                    menu.findItem(R.id.menu_track).setTitle(generalFunc.retrieveLangLBl("JOB PROGRESS", " LBL_PROGRESS_HINT")).setVisible(false);
                    onGoingTripsDetailListRecyclerView.setVisibility(View.VISIBLE);
                    progressHinttext.setText(generalFunc.retrieveLangLBl("JOB PROGRESS", "LBL_PROGRESS_HINT"));
                    googlemaparea.setVisibility(View.GONE);
                    timeTxt.setVisibility(View.GONE);
                    invalidateOptionsMenu();
                }
            }


            menu.findItem(R.id.menu_sos).setTitle(generalFunc.retrieveLangLBl("Emergency or SOS", "LBL_EMERGENCY_SOS_TXT"));

            Utils.setMenuTextColor(menu.findItem(R.id.menu_cancel_trip), getResources().getColor(R.color.appThemeColor_TXT_1));
            Utils.setMenuTextColor(menu.findItem(R.id.menu_view_tutor_detail), getResources().getColor(R.color.appThemeColor_TXT_1));
            Utils.setMenuTextColor(menu.findItem(R.id.menu_sos), getResources().getColor(R.color.appThemeColor_TXT_1));
            Utils.setMenuTextColor(menu.findItem(R.id.menu_track), getResources().getColor(R.color.appThemeColor_TXT_1));
            Utils.setMenuTextColor(menu.findItem(R.id.menu_call), getResources().getColor(R.color.appThemeColor_TXT_1));
            Utils.setMenuTextColor(menu.findItem(R.id.menu_message), getResources().getColor(R.color.appThemeColor_TXT_1));


        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_view_tutor_detail:
                new OpenTutorDetailDialog(getActContext(), tripDetail, generalFunc);
                return true;

            case R.id.menu_cancel_trip:
                buildWarningMessage(generalFunc.retrieveLangLBl("Are you sure, you want to cancel your Job?", "LBL_BOOKING_CANCEL_TXT"),
                        generalFunc.retrieveLangLBl("", "LBL_YES"),
                        generalFunc.retrieveLangLBl("Continue Job", "LBL_NO"), true);
                return true;
            case R.id.menu_sos:
                Bundle bn = new Bundle();
                // bn.putString("UserProfileJson", userProfileJson);
                bn.putString("TripId", tripDetail.get("iTripId"));
                new StartActProcess(getActContext()).startActWithData(ConfirmEmergencyTapActivity.class, bn);

                return true;

            case R.id.menu_track:

                if (item.getTitle().toString().equalsIgnoreCase(generalFunc.retrieveLangLBl("Live Track", "LBL_LIVE_TRACK_TXT"))) {
                    item.setTitle(generalFunc.retrieveLangLBl("JOB PROGRESS", " LBL_PROGRESS_HINT"));
                    onGoingTripsDetailListRecyclerView.setVisibility(View.GONE);
                    progressHinttext.setText(generalFunc.retrieveLangLBl("Live Tarck", "LBL_LIVE_TRACK_TXT"));
                    googlemaparea.setVisibility(View.VISIBLE);
                    if (timeTxt.length() > 0) {
                        timeTxt.setVisibility(View.VISIBLE);
                    } else {
                        timeTxt.setVisibility(View.GONE);
                    }

                } else {
                    item.setTitle(generalFunc.retrieveLangLBl("Live Tarck", "LBL_LIVE_TRACK_TXT"));
                    onGoingTripsDetailListRecyclerView.setVisibility(View.VISIBLE);
                    progressHinttext.setText(generalFunc.retrieveLangLBl("JOB PROGRESS", "LBL_PROGRESS_HINT"));
                    googlemaparea.setVisibility(View.GONE);
                    timeTxt.setVisibility(View.GONE);


                }


                return true;

            case R.id.menu_call:

                call("+" + tripDetail.get("vCode") + "" + tripDetail.get("driverMobile"));

                return true;
            case R.id.menu_message:
//                try {
//                    Intent smsIntent = new Intent(Intent.ACTION_VIEW);
//                    smsIntent.setType("vnd.android-dir/mms-sms");
//                    smsIntent.putExtra("address", "" + tripDetail.get("vCode") + "" + tripDetail.get("driverMobile"));
//                    getActContext().startActivity(smsIntent);
//                } catch (Exception e) {
//
//                }

                Bundle bnChat = new Bundle();
                bnChat.putString("iFromMemberId", tripDetail.get("iDriverId"));
                bnChat.putString("FromMemberImageName", tripDetail.get("driverImage"));
                bnChat.putString("iTripId", tripDetail.get("iTripId"));
                bnChat.putString("FromMemberName", tripDetail.get("driverName"));

                new StartActProcess(getActContext()).startActWithData(ChatActivity.class, bnChat);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void call(String phoneNumber) {
        try {

            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + phoneNumber));
            startActivity(callIntent);

        } catch (Exception e) {
            // TODO: handle exception
        }


    }


    public void buildWarningMessage(String message, String positiveBtn, String negativeBtn, final boolean isCancelTripWarning) {

        android.support.v7.app.AlertDialog alertDialog;
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActContext());
        builder.setTitle("");
        builder.setCancelable(false);
        builder.setMessage(message);

        builder.setPositiveButton(positiveBtn, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                if (isCancelTripWarning == true) {
                    cancelTrip("No");
                } else {
                }
            }
        });
        builder.setNegativeButton(negativeBtn, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });

        alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

    }

    public void cancelTrip(String eConfirmByUser) {


        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "cancelTrip");
        parameters.put("UserType", CommonUtilities.app_type);
        parameters.put("iUserId", generalFunc.getMemberId());
        parameters.put("iDriverId", tripDetail.get("iDriverId"));
        parameters.put("iTripId", tripDetail.get("iTripId"));
        parameters.put("eConfirmByUser", eConfirmByUser);

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(getActContext(), parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(CommonUtilities.action_str, responseString);

                    if (isDataAvail == true) {
                        // finish();
                        GenerateAlertBox generateAlert = new GenerateAlertBox(getActContext());
                        generateAlert.setCancelable(false);
                        generateAlert.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
                            @Override
                            public void handleBtnClick(int btn_id) {

                                generalFunc.restartwithGetDataApp();
                            }
                        });

                        generateAlert.setContentMessage("", generalFunc.retrieveLangLBl("", generalFunc.getJsonValue("message1", responseString)));
                        generateAlert.setPositiveBtn(generalFunc.retrieveLangLBl("", "LBL_BTN_OK_TXT"));
                        generateAlert.showAlertBox();

                    } else {
                        if (generalFunc.getJsonValue("isCancelChargePopUpShow", responseString).equalsIgnoreCase("Yes")) {

                            final GenerateAlertBox generateAlert = new GenerateAlertBox(getActContext());
                            generateAlert.setCancelable(false);
                            generateAlert.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
                                @Override
                                public void handleBtnClick(int btn_id) {
                                    if (btn_id == 0) {
                                        generateAlert.closeAlertBox();

                                    } else {
                                        generateAlert.closeAlertBox();
                                        cancelTrip("Yes");

                                    }

                                }
                            });
                            generateAlert.setContentMessage("", generalFunc.retrieveLangLBl("", generalFunc.getJsonValue(CommonUtilities.message_str, responseString)));
                            generateAlert.setPositiveBtn(generalFunc.retrieveLangLBl("", "LBL_YES"));
                            generateAlert.setNegativeBtn(generalFunc.retrieveLangLBl("", "LBL_NO"));
                            generateAlert.showAlertBox();

                            return;
                        }

                        buildWarningMessage(generalFunc.retrieveLangLBl("", "LBL_REQUEST_FAILED_PROCESS"),
                                generalFunc.retrieveLangLBl("", "LBL_BTN_OK_TXT"), "", false);
                    }
                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

    public void getTripDeliveryLocations() {
        if (errorView.getVisibility() == View.VISIBLE) {
            errorView.setVisibility(View.GONE);
        }
        loading_ongoing_trips_detail.setVisibility(View.VISIBLE);
        final HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "getTripDeliveryLocations");
        parameters.put("iTripId", tripDetail.get("iTripId"));
        parameters.put("iCabBookingId", "");

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(getActContext(), parameters);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {

                    closeLoader();

                    if (generalFunc.checkDataAvail(CommonUtilities.action_str, responseString) == true) {
                        list = new ArrayList<>();

                        String message = generalFunc.getJsonValue(CommonUtilities.message_str, responseString);
                        server_time = generalFunc.getJsonValue("SERVER_TIME", responseString);
                        String driverDetails = generalFunc.getJsonValue("driverDetails", message);

                        destLoc = new Location("Dest");
                        destLoc.setLatitude(GeneralFunctions.parseDoubleValue(0.0, generalFunc.getJsonValue("tStartLat", driverDetails)));
                        destLoc.setLongitude(GeneralFunctions.parseDoubleValue(0.0, generalFunc.getJsonValue("tStartLong", driverDetails)));


                        eType = generalFunc.getJsonValue("eType", driverDetails);

                        driverStatus = generalFunc.getJsonValue("driverStatus", driverDetails);

                        JSONArray tripLocations = generalFunc.getJsonArray("States", message);

                        if (driverStatus.equals("Arrived")) {
                            isarrived = true;

                        } else {
                            subscribeToDriverLocChannel();
                            isarrived = false;
                        }


                        list.clear();
                        if (tripLocations != null)
                            for (int i = 0; i < tripLocations.length(); i++) {
                                tempMap = new HashMap<>();

                                JSONObject jobject1 = generalFunc.getJsonObject(tripLocations, i);
                                tempMap.put("status", generalFunc.getJsonValue("type", jobject1.toString()));
                                tempMap.put("iTripId", generalFunc.getJsonValue("text", jobject1.toString()));
                                tempMap.put("value", generalFunc.getJsonValue("timediff", jobject1.toString()));
                                tempMap.put("Booking_LBL", generalFunc.retrieveLangLBl("", "LBL_BOOKING"));
                                tempMap.put("time", generalFunc.getJsonValue("time", jobject1.toString()));
                                tempMap.put("msg", generalFunc.getJsonValue("text", jobject1.toString()));

                                list.add(tempMap);
                            }
                        setView();
                    } else {
                        generateErrorView();
                    }
                } else {
                    generateErrorView();
                }
            }
        });
        exeWebServer.execute();
    }

    private void setView() {
        onGoingTripDetailAdapter = new OnGoingTripDetailAdapter(getActContext(), list, generalFunc);
        onGoingTripsDetailListRecyclerView.setAdapter(onGoingTripDetailAdapter);
        onGoingTripDetailAdapter.notifyDataSetChanged();

    }

    public void generateErrorView() {

        closeLoader();

        generalFunc.generateErrorView(errorView, "LBL_ERROR_TXT", "LBL_NO_INTERNET_TXT");

        if (errorView.getVisibility() != View.VISIBLE) {
            errorView.setVisibility(View.VISIBLE);
        }
        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                getTripDeliveryLocations();
            }
        });
    }

    public void closeLoader() {
        if (loading_ongoing_trips_detail.getVisibility() == View.VISIBLE) {
            loading_ongoing_trips_detail.setVisibility(View.GONE);
        }
    }

    private void setDriverDetail() {

        String image_url = CommonUtilities.SERVER_URL_PHOTOS + "upload/Driver/" + tripDetail.get("iDriverId") + "/"
                + tripDetail.get("driverImage");

        Picasso.with(getActContext())
                .load(image_url)
                .placeholder(R.mipmap.ic_no_pic_user)
                .error(R.mipmap.ic_no_pic_user)
                .into(((ImageView) findViewById(R.id.user_img)));

        userNameTxt.setText(tripDetail.get("driverName"));
        userAddressTxt.setText(tripDetail.get("tSaddress"));
        ratingBar.setRating(generalFunc.parseFloatValue(0, tripDetail.get("driverRating")));

    }

    public void setTimetext(String distance, String time) {
        try {
            String userProfileJson = generalFunc.retrieveValue(CommonUtilities.USER_PROFILE_JSON);
            String distance_str = "";
            if (generalFunc.retrieveValue(APP_TYPE).equalsIgnoreCase("UberX") || eType.equalsIgnoreCase(Utils.CabGeneralType_UberX)) {

                if (googlemaparea.getVisibility() == View.VISIBLE) {
                    timeTxt.setVisibility(View.VISIBLE);
                }


                Utils.printLog("eUnit", "::" + generalFunc.getJsonValue("eUnit", userProfileJson));
                if (userProfileJson != null && !generalFunc.getJsonValue("eUnit", userProfileJson).equalsIgnoreCase("KMs")) {
                    timeTxt.setText(time + " " + generalFunc.retrieveLangLBl("to reach", "LBL_REACH_TXT") + " & " + distance + " " + generalFunc.retrieveLangLBl("", "LBL_MILE_DISTANCE_TXT") + " " + generalFunc.retrieveLangLBl("away", "LBL_AWAY_TXT"));
                } else {
                    timeTxt.setText(time + " " + generalFunc.retrieveLangLBl("to reach", "LBL_REACH_TXT") + " & " + distance + " " + generalFunc.retrieveLangLBl("", "LBL_KM_DISTANCE_TXT") + " " + generalFunc.retrieveLangLBl("away", "LBL_AWAY_TXT"));

                }


            } else {
                if (tripDetail.get("eFareType").equalsIgnoreCase(Utils.CabFaretypeRegular)) {

                    timeTxt.setVisibility(View.VISIBLE);
                } else {
                    timeTxt.setVisibility(View.GONE);
                }

            }
        } catch (Exception e) {

        }

    }

    private void setLables() {

        titleTxt.setText(generalFunc.retrieveLangLBl("Booking No", "LBL_BOOKING") + "# " + tripDetail.get("vRideNo"));
        subTitleTxt.setVisibility(View.VISIBLE);
        subTitleTxt.setText(generalFunc.retrieveLangLBl("Live Track", "LBL_LIVE_TRACK_TXT"));
        subTitleTxt.setVisibility(View.GONE);

    }

    private void init() {
        titleTxt = (MTextView) findViewById(R.id.titleTxt);
        backImgView = (ImageView) findViewById(R.id.backImgView);
        subTitleTxt = (MTextView) findViewById(R.id.subTitleTxt);

        loading_ongoing_trips_detail = (ProgressBar) findViewById(R.id.loading_ongoing_trips_detail);
        onGoingTripsDetailListRecyclerView = (RecyclerView) findViewById(R.id.onGoingTripsDetailListRecyclerView);
        contentArea = (LinearLayout) findViewById(R.id.contentArea);
        errorView = (ErrorView) findViewById(R.id.errorView);
        user_img = (SelectableRoundedImageView) findViewById(R.id.user_img);
        userNameTxt = (MTextView) findViewById(R.id.userNameTxt);
        userAddressTxt = (MTextView) findViewById(R.id.userAddressTxt);
        ratingBar = (SimpleRatingBar) findViewById(R.id.ratingBar);
        progressHinttext = (MTextView) findViewById(R.id.progressHinttext);
        timeTxt = (MTextView) findViewById(R.id.timeTxt);

        googlemaparea = (LinearLayout) findViewById(R.id.googlemaparea);
        generalFunc = new GeneralFunctions(getActContext());
        backImgView.setOnClickListener(new setOnClickList());
        subTitleTxt.setOnClickListener(new setOnClickList());


        map.setListener(new CustomSupportMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
            }
        });


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        setTaskKilledValue(true);


        ConfigPubNub.getInstance().releaseInstances();
        unSubscribeToDriverLocChannel();

    }

    private Activity getActContext() {
        return OnGoingTripDetailsActivity.this;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.gMap = googleMap;

        this.gMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return true;
            }
        });
    }

    @Override
    public void onTaskRun() {


        updateDriverLocations();
    }

    public void updateDriverLocations() {
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "getDriverLocations");
        parameters.put("iDriverId", tripDetail.get("iDriverId"));
        parameters.put("UserType", CommonUtilities.app_type);

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(getActContext(), parameters);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(CommonUtilities.action_str, responseString);

                    if (isDataAvail == true) {

                        String vLatitude = generalFunc.getJsonValue("vLatitude", responseString);
                        String vLongitude = generalFunc.getJsonValue("vLongitude", responseString);
                        String vTripStatus = generalFunc.getJsonValue("vTripStatus", responseString);

                        if (vTripStatus.equals("Arrived")) {
                            isarrived = true;
                            invalidateOptionsMenu();


                        }

                        LatLng driverLocation_update = new LatLng(generalFunc.parseDoubleValue(0.0, vLatitude),
                                generalFunc.parseDoubleValue(0.0, vLongitude));

                        if (driverMarker != null) {
                            driverMarker.remove();
                        }
                        driverLocation = driverLocation_update;


                        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.mipmap.car_driver).copy(Bitmap.Config.ARGB_8888, true);
                        driverMarker = gMap.addMarker(
                                new MarkerOptions().position(driverLocation)
                                        .icon(BitmapDescriptorFactory.fromBitmap(bm)));

                        driverMarker.setFlat(true);
                        driverMarker.setAnchor(0.5f, 1);


                        /*}*/

                        CameraPosition cameraPosition = cameraForDriverPosition();
                        gMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    }
                } else {
//                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

    public CameraPosition cameraForDriverPosition() {

        double currentZoomLevel = gMap == null ? Utils.defaultZomLevel : gMap.getCameraPosition().zoom;

        if (Utils.defaultZomLevel > currentZoomLevel) {
            currentZoomLevel = Utils.defaultZomLevel;
        }
        if (driverLocation != null) {
            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(this.driverLocation.latitude, this.driverLocation.longitude))
                    .zoom((float) currentZoomLevel).build();


            return cameraPosition;
        } else {
            return null;
        }
    }

    public void updateDriverLocation(LatLng latlog) {
        if (latlog == null) {
            return;
        }

        if (driverMarker == null) {
            try {
                if (gMap != null) {
                    marker_view = ((LayoutInflater) getActContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                            .inflate(R.layout.ufx_live_track, null);
                    providerImgView = (SelectableRoundedImageView) marker_view
                            .findViewById(R.id.providerImgView);


                    driverMarker = gMap.addMarker(
                            new MarkerOptions().position(latlog)
                                    .title("DriverId" + tripDetail.get("iDriverId")).
                                    icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(getActContext(), marker_view)))
                                    .anchor(0.5f, 1.0f).flat(true));


                    driverLocation = latlog;
                    CameraPosition cameraPosition = cameraForDriverPosition();
                    gMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


                    providerImgView.setImageResource(R.mipmap.pdefault);

                    driverMarker.setIcon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(getActContext(), marker_view)));


                }
            } catch (Exception e) {
                Utils.printLog("markerException", e.toString());
            }
        } else {
            double currentZoomLevel = gMap.getCameraPosition().zoom;

            if (Utils.defaultZomLevel > currentZoomLevel) {
                currentZoomLevel = Utils.defaultZomLevel;
            }
            CameraPosition cameraPosition = new CameraPosition.Builder().target(latlog)
                    .zoom((float) currentZoomLevel).build();

            gMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


            Location location = new Location("livetrack");
            location.setLatitude(latlog.latitude);
            location.setLongitude(latlog.longitude);
            animDriverMarker.animateMarker(driverMarker, gMap, location, 0, 800, tripDetail.get("iDriverId"), "");

        }


    }

    public void pubNubMsgArrived(final String message, final Boolean ishow) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                String msgType = generalFunc.getJsonValue("MsgType", message);
                String iDriverId = generalFunc.getJsonValue("iDriverId", message);

                if (tripDetail.get("iDriverId").equals(iDriverId)) {
                    if (msgType.equals("LocationUpdateOnTrip")) {
                        String vLatitude = generalFunc.getJsonValue("vLatitude", message);
                        String vLongitude = generalFunc.getJsonValue("vLongitude", message);

                        Location driverLoc = new Location("Driverloc");
                        driverLoc.setLatitude(generalFunc.parseDoubleValue(0.0, vLatitude));
                        driverLoc.setLongitude(generalFunc.parseDoubleValue(0.0, vLongitude));


                        if (!isarrived) {
                            callUpdateDeirection(driverLoc);
                        } else {
                            timeTxt.setVisibility(View.GONE);
                        }

                        LatLng driverLocation_update = new LatLng(generalFunc.parseDoubleValue(0.0, vLatitude),
                                generalFunc.parseDoubleValue(0.0, vLongitude));

                        driverLocation = driverLocation_update;

                        if (googlemaparea.getVisibility() == View.VISIBLE) {
                            updateDriverLocation(driverLocation_update);
                        }
                    } else if (msgType.equals("DriverArrived")) {

                        isarrived = true;
                        invalidateOptionsMenu();

                        if (!isarrivedpopup) {
                            isarrivedpopup = true;

                            getTripDeliveryLocations();


                        }

                    } else {

                        onGcmMessageArrived(message, ishow);
                    }


                }


            }
        });
    }


    public void onGcmMessageArrived(final String message, boolean ishow) {

        String driverMsg = generalFunc.getJsonValue("Message", message);

        if (driverMsg.equals("DriverArrived")) {
            getTripDeliveryLocations();

        }


        if (driverMsg.equals("TripEnd")) {

// show alert - - not cancabable - ok button only - on click OK finish screen
            if (ishow) {
                if (!ishowdialog) {
                    ishowdialog = true;
                    GenerateAlertBox alertBox = new GenerateAlertBox(getActContext());
                    alertBox.setContentMessage("", generalFunc.getJsonValue("vTitle", message));
                    alertBox.setPositiveBtn(generalFunc.retrieveLangLBl("Ok", "LBL_BTN_OK_TXT"));


                    alertBox.setCancelable(false);
                    alertBox.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
                        @Override
                        public void handleBtnClick(int btn_id) {

                            Utils.ClerAllNotification();
                            Bundle bn = new Bundle();
                            bn.putBoolean("isUfx", true);
                            bn.putString("iTripId", generalFunc.getJsonValue("iTripId", message));
                            new StartActProcess(getActContext()).startActWithData(RatingActivity.class, bn);
                            finish();


                        }
                    });
                    alertBox.showAlertBox();
                }
            }

        } else if (driverMsg.equals("TripStarted")) {

            getTripDeliveryLocations();


        } else if (driverMsg.equals("TripCancelledByDriver")) {
// show alert - - not cancabable - ok button only - on click OK finish screen

            if (ishow) {
                if (!ishowdialog) {
                    ishowdialog = true;
                    String reason = generalFunc.getJsonValue("Reason", message);


                    GenerateAlertBox alertBox = new GenerateAlertBox(getActContext());
                    alertBox.setContentMessage("", generalFunc.retrieveLangLBl("", "LBL_PREFIX_TRIP_CANCEL_DRIVER") + " " + reason);
                    alertBox.setPositiveBtn(generalFunc.retrieveLangLBl("Ok", "LBL_BTN_OK_TXT"));

                    alertBox.setCancelable(false);
                    alertBox.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
                        @Override
                        public void handleBtnClick(int btn_id) {

                            if (generalFunc.getJsonValue("ShowTripFare", message).equalsIgnoreCase("true")) {
                                Utils.ClerAllNotification();
                                Bundle bn = new Bundle();
                                bn.putBoolean("isUfx", true);
                                bn.putString("iTripId", generalFunc.getJsonValue("iTripId", message));
                                new StartActProcess(getActContext()).startActWithData(RatingActivity.class, bn);
                                finish();
                            } else {
                                backImgView.performClick();
                            }

                        }
                    });

                    alertBox.showAlertBox();
                }
            }
        }
    }

    public class setOnClickList implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.backImgView) {
                OnGoingTripDetailsActivity.super.onBackPressed();
            } else if (view.getId() == R.id.subTitleTxt) {
                generalFunc.autoLogin(OnGoingTripDetailsActivity.this, tripDetail.get("iTripId"));
            }


        }
    }
}
